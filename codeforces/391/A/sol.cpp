#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int main(){
	FASTER;

	string s;

	cin >> s;

	int F[300];
	int B[300];

	MEM(F,0);
	MEM(B,0);


	string bul = "Bulbasaur";

	for (int i = 0; i < s.size(); ++i) {
		F[s[i]]++;
	}


	for (int i = 0; i < bul.size(); ++i) {
		B[bul[i]]++;
	}


	int ans = s.size()+1;


	for (int i = 0; i < bul.size(); ++i) {
//		printf("B[%c] = %d\n",bul[i],B[bul[i]]);
		ans = min(ans, F[bul[i]] / B[bul[i]]);
	}
	cout << ans << endl;

	return 0;
}
