#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//
#define MOD 1000000007
#define MAXN 80

int dp[MAXN][(1 << 20) + 10];
string s;
int n;

inline int add(int a, int b){
	int ret = a + b;
	while(ret >= MOD)ret-=MOD;
	return ret;
}

int sol(int i, int mask){

	int x = __builtin_popcount(mask);

	int &ret = dp[i][mask];

	if(i == n){

		if(x != 0 && ((1<<x)-1) == mask)
			ret = 1;
		else
			ret = 0;
	}

	if(ret == -1){
		ret = 0;

		if(s[i] == '0'){
			ret = sol(i+1,mask);
		}else{

			int B = 0;
			for (int j = i; j < n; ++j) {
				B *=2;
				B += (s[j]-'0');

				if(B>20)break;

				int tmp = sol(j+1, mask | (1 << (B-1)));

				ret = add(ret, tmp);
			}
			if(x != 0 && (1<<x)-1 == mask)ret++;
		}
	}



	return ret;
}

int main(){
	FASTER;

	cin >> n >> s;

	MEM(dp, -1);

	int ans = 0;
	for (int i = 0; i < n; ++i) {
		ans = add(ans, sol(i,0));
	}

	cout << ans << endl;

	return 0;
}
