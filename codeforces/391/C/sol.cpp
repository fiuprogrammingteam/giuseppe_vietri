#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//
#define MAXN 1234567

int n,m;
vi G[MAXN];

int main(){
	FASTER;
	cin >> n >> m;


	for (int i = 0; i < n; ++i) {
		int q;
		cin >> q;
		for (int j = 0; j < q; ++j) {
			int x ; cin >> x;
			G[x].push_back(i);
		}
	}

	sort(G+1,G + m+1);

	int k = 1;
	ll ans = 1;

	for (int i = 2; i <= m; ++i) {
		if(G[i] == G[i-1]){
			k++;
		}else{
			k=1;
		}
		ans = ans * k % 1000000007;
	}

	cout << ans << endl;

	return 0;
}
