#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//
#define MAXN 200100
int n;
int A[MAXN];
int P[MAXN];
int Poke[MAXN];
int main(){
	FASTER;

	cin >> n;

	MEM(P,0);
	MEM(Poke,0);

	for (int i = 0; i < n; ++i) {
		cin >> A[i];
		Poke[A[i]]++;
	}

	for (int i = 0; i < MAXN; ++i) {
		P[i]=1;
	}

	P[0]=0;
	P[1]=0;

	int ans = Poke[1] != 0;

	for (ll i = 2; i < MAXN; ++i) {
		int tmp = 0;
		for (ll j = i; j < MAXN; j+=i) {
			tmp += Poke[j];
		}
		ans = max(ans,tmp);

//		if(P[i]){
//
//			int tmp = 0;
//
//			for (ll j = i; j < MAXN; j+=i) {
//				tmp += Poke[j];
//			}
//
//			for (ll j = 1ll*i*i; j < 1ll*MAXN; j+=i) {
//				P[j]=0;
//			}
//
//			ans = max(ans,tmp);
//
//		}

	}

	cout << ans << endl;

	return 0;
}
