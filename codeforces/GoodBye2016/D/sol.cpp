#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

#define North 0
#define South0
#define East 0
#define West 0
#define MAXN 333

int G[MAXN][MAXN][8];
int NG[MAXN][MAXN][8];
int A[MAXN][MAXN];

int _r[8]  = {-1, -1, 0, 1, 1, 1, 0, -1};
int _c[8]  = {0, 1, 1, 1, 0, -1, -1, -1};

int main(){
	FASTER;
    int n;
    int cnt;
	cin >> n;

    G[MAXN/2][MAXN/2][0]=1;
    A[MAXN/2][MAXN/2]=1;
    MEM(A,0);

	for (int test  = 0; test < n; ++test) {
		cin >> cnt;

        for(int it = 0 ; it < cnt ; it++){
            MEM(NG,0);
            for(int i = 0 ; i < MAXN ; i ++)
                for(int j = 0 ; j < MAXN ; j++){
                    for(int d = 0 ; d < 8 ; d++){
                        if(G[i][j][d]){
                            NG[i+_r[d]][j+_c[d]][d] = 1;
                        }
                    }
                }

            for(int i = 0 ; i < MAXN ; i ++)
                for(int j = 0 ; j < MAXN ; j++){
                    for(int d = 0 ;d < 8 ; d++){
                        G[i][j][d] = NG[i][j][d];
                        A[i][j] |= G[i][j][d] ;
                    }
                }
        }


        MEM(NG,0);
        for(int i = 0 ; i < MAXN ; i ++)
            for(int j = 0 ; j < MAXN ; j++){
                for(int d = 0 ;d < 8 ; d++){
                    if(G[i][j][d]){
                        NG[i][j][(d+1)%8]=1;
                        NG[i][j][(d+7)%8]=1;
                    }
                }
            }
 
         for(int i = 0 ; i < MAXN ; i ++)
             for(int j = 0 ; j < MAXN ; j++){
                 for(int d = 0 ;d < 8 ; d++){
                     G[i][j][d] = NG[i][j][d];
                 }
            }            
	}

    int ans = 0;

     for(int i = 0 ; i < MAXN ; i ++)
             for(int j = 0 ; j < MAXN ; j++){
                 ans += A[i][j];
             }
     cout << ans << endl;

	return 0;
}
