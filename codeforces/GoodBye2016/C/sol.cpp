#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int main(){
	FASTER;
	int n;
	cin >> n;

	int sum = 0;
	vi R(n+1,0);
	vi D(n+1,0);

	for (int i = 0; i < n; ++i) {
		int r,d;
		cin >> r >> d;
		R[i+1]= r + R[i];
		D[i]=d;
	}

	int r = 1000000000;
	for (int i = 0; i < n; ++i) {
		if(D[i]==2){
			r = min(r, 1900 - R[i]-1);
		}
	}

	for (int i = 0; i < n; ++i) {
		if(D[i]==1){
			if(r + R[i] < 1900){
				cout << "Impossible\n";
				return 0;
			}
		}
	}

	if(r == 1000000000)
		cout << "Infinity\n";
	else
		cout << r + R[n] << endl;

	return 0;
}
