#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int _r1[8] = { 1, 1, 1,-1,-1,-1, 0, 0};
int _c1[8] = { 1,-1, 0, 1,-1, 0, 1,-1};
int _r2[8] = { 2, 2, 2,-2,-2,-2, 0, 0};
int _c2[8] = { 2,-2, 0, 2,-2, 0, 2,-2};

vector<string> B;

bool check(int r, int c){


	for (int i = 0; i < 8; ++i) {

		int r1 = r + _r1[i];
		int c1 = c + _c1[i];
		int r2 = r + _r2[i];
		int c2 = c + _c2[i];



		if(r2<0||r2>=4||c2<0||c2>=4)continue;


		int xs = (B[r][c]=='x') + (B[r1][c1]=='x') + (B[r2][c2]=='x');
		int dots = (B[r][c]=='.') + (B[r1][c1]=='.') + (B[r2][c2]=='.');

		if(xs == 2 && dots == 1){
//			printf("%d %d   %d %d   %d %d\n",r,c,r1,c1,r2,c2);
//			printf("%d %d %d\n",r,c,i);
			return true;
		}

	}

	return false;
}

int main(){
	FASTER;

	for (int i = 0; i < 4; ++i) {
		string s;
		cin >> s;
		B.push_back(s);
	}

	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 4; ++j) {



			if(check(i,j)){

				cout << "YES\n";
				return 0;
			}

		}
	}

	cout << "NO\n";

	return 0;
}
