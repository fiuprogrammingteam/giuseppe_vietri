#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int n,k;
vii in;
vi id;

int solve(int ans = -1){

    priority_queue<ii> q;

    if(ans == 0){
        printf("0\n");
        for(int i = 0; i < k ; i++)printf("%d ",i+1);
        printf("\n");
        return 0;
    }

//    for(int i = 0 ; i < n ; i++){
//    	printf("%d %d   %d\n", in[i].first, in[i].second, id[i]+1);
//    }

    int nans = 0;

    for(int i = 0 ; i < n ; i++){
    
        q.push(ii(-in[i].second, id[i]));

        if(q.size() > k)q.pop();
        if(q.size()==k){
            
            int tmp = -q.top().first - in[i].first+1;

//            printf("i = %d\t%d %d\ttmp = %d ans = %d\n",i,in[i].first,-q.top().first,tmp ,nans);

            if(ans != -1){
                if(ans == tmp){
                    printf("%d\n",ans);
                    while(!q.empty()){
                        printf("%d ",q.top().second+1);
                        q.pop();
                    }
                    printf("\n");
                    return 0;
                }
            }else{
                if(tmp > nans){
                    nans = tmp;
                }
            }
        }
    }
    return nans;
}

bool cmpid(int i, int j){
    return in[i] < in[j];
}

int main(){
	FASTER;

	cin >> n >> k;

	for (int i = 0; i < n; ++i) {
		int l,r;
		cin >> l >> r;
		in.push_back(ii(l,r));
        id.push_back(i);
	}


    sort(id.begin(), id.end(),cmpid);
    sort(in.begin(), in.end());



    solve(solve());
    return 0;
}
