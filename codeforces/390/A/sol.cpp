#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int a[200];
int L[200];


int main(){
	FASTER;

	int n;
	cin >> n;
	int id = 1;
	for (int i = 0; i < n; ++i) {
		cin >> a[i];
	}


	for (int i = 0; i < n; ++i) {

		if(a[i] == 0){
			if(i){
				L[i] = L[i-1];
			}else{

				while(i<n && a[i]==0){
					L[i++]=id;
				}

				if(i>=n){
					cout << "NO\n";
					return 0;
				}

				L[i]=id++;
			}
		}else{
			L[i]=id++;
		}

	}


//	for (int i = 0; i < n; ++i) {
//		printf("%d ", L[i]);
//	}
//	printf("\n");

	cout << "YES\n";
	cout << id-1 << endl;
	for (int i = 0,j = 0 , k = 0; i < id-1; ++i) {
		j = k;

		while(k<n && L[k] == L[j]){
			k++;
		}

		cout << j +1<< " " << k << endl;

	}

	return 0;
}
