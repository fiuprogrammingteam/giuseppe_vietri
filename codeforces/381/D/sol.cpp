#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<ll,ll> ii;
typedef pair<ii,ll> iii;
typedef vector<ll> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//
#define MAXN ((ll)(2e9 + 10))
int n;
vi a;
vector<vii> T;
vi In,Out,st,dist;
int cnt = 0;

void dfs(int u, ll di, int p = -1){
	In[u] = cnt++;
	dist[u] = di;
	for(int i = 0 ; i < T[u].size() ; i++){
		int v = T[u][i].first;
		ll w = T[u][i].second;
		if(v != p){
			dfs(v,di + w, u);
		}
	}
	Out[u] = cnt++;
}

int sum(int i){
	int s = 0;
	for(;i>0;i -= i&(-i)){
		s += st[i];
	}
	return s;
}

void upt(int i, int va){
	for(;i<=2*n+10;i += i&(-i)){
		st[i] += va;
	}
}

int main(){
	FASTER;
	cin >> n;
	a.assign(n,0);
	T.assign(n,vii());
	In.assign(2*n+10,0);
	Out.assign(2*n+10,0);
	st.assign(2*n+10,0);
	dist.assign(n,0);

	for(int i = 0 ; i < n ; i++)
		cin >> a[i];

	for (int i = 0; i < n-1; ++i) {
		int p , w;
		cin >> p >> w;
		p--;
		T[i+1].push_back(ii(p, w));
		T[p].push_back(ii(i+1, w));
	}

	cnt = 1;
	dfs(0,0);

	vector<iii> E;
	for (int i = 0; i < n; ++i) {
		E.push_back(iii(ii(dist[i] - a[i], 0), i));
		E.push_back(iii(ii(dist[i]		 , 1), i));
	}

	sort(E.begin(), E.end());
	vi ans(n,0);
	for (int i = 0; i < E.size(); ++i) {

		int ty = E[i].first.second;
		int id = E[i].second;

		//printf("di = %d\n", di);
		if(ty == 0){
			//printf("In[%d] = %d\n", id, In[id]);
			upt(In[id], 1);
		}else{
			int out = sum(Out[id]);
			int in  = (In[id] ? sum(In[id]-1) : 0);
			int s = out - in; 
			
			//printf("id = %d: [%d,%d]\n",id, in, out);
			ans[id] = s-1;
		}
	}

	for(int i =  0 ; i < n ; i++)cout << ans[i] << " ";
	cout << endl;

	return 0;
}
