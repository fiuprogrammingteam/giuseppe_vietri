#include <bits/stdc++.h>
using namespace std;

const int  INF = std::numeric_limits<int>::max()/3;

#define max(a,b)(a>b?a:b)
#define min(a,b)(a<b?a:b)
#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI (acos(0)*2.0)
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b&(-b)) // Least significant bit

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;


// ---------------------------------------------------------------------------------------
#define MAX_V 100
#define MAX_E 100


class Edge{
public:
	int u,v,cap,f;
	double cost;

	Edge(int _u, int _v, int _cap, double _c) : u(_u),v(_v),cap(_cap),cost(_c){
		f = 0;
	}

	double getCost(int w){
		if(w == u)return - cost;
		return cost;
	}

	int other(int vertex){
		if(vertex == u)return v;
		else if(vertex == v)return u;
		return -1;
	}

	int residualTo(int vertex){
		if(vertex == u)return f;
		return cap - f;
	}

	void addFlowTo(int vertex, int ff){
		if(vertex == u){
			f -= ff;
		}else if(vertex == v){
			f += ff;
		}
	}
};


typedef vector<Edge*> edgeList;

int n;
int SRC = 0;
int SINK = 1;
int Flow;
int poke, ultra;
edgeList parent;
vi visited;
vector<edgeList> adj;
set<int> vertices;
vector<double> dist;

void addEdge(int u, int v, int cap, double cost){
	//printf("addEdge %d %d %d %lf\n", u,v,cap,cost);
	Edge* e = new Edge(u,v,cap,cost);
	adj[u].push_back(e);
	adj[v].push_back(e);
	vertices.insert(u);
	vertices.insert(v);
}

// Bellman Ford Algorithm
bool SSSP(){
	dist.assign(SINK+10,INF);
	parent.assign(SINK+10 , NULL);
	dist[SRC] = 0;
	for (int i = 0; i < vertices.size() -1; ++i) {
		for (set<int>::iterator it = vertices.begin() ; it != vertices.end() ; ++it) {
			int u = *it;
			if(dist[u] < INF)
				for (int k = 0; k < (int)adj[u].size(); ++k) {
					Edge *e = adj[u][k];
					int v = e->other(u);
					if(e->residualTo(v) > 0 && dist[u] + e->getCost(v) < dist[v]){
						parent[v] = e;
						dist[v] = dist[u] + e->getCost(v);
					}
				}
		}
	}
	return dist[SINK] != INF;
}


// Ford Fulkerson
double maxFlow(){

	for (int i = 0; i < adj.size(); ++i) {
		for (int j = 0; j < adj[i].size(); ++j) {
			adj[i][j]->f = 0;
		}
	}

	int mf = 0;
	while(SSSP()){
		Flow = INF;

		for(int v = SINK; v != SRC ; v = parent[v]->other(v)){
			//printf("v = %d\n", v);
			Flow = min(Flow, parent[v]->residualTo(v));
		}
		//printf("\n");

		for(int v = SINK; v != SRC ; v = parent[v]->other(v)){
			parent[v]->addFlowTo(v,Flow);
		}
		mf += Flow;
	}

	double cost = 0;

	for(int i = 0 ; i < adj[SINK].size()  ; i++){
		Edge * e = adj[SINK][i];
		int v = e->other(SINK);
		if(e->residualTo(SINK) == 0){
			cost += -e->getCost(SINK);	
			//printf("%d -> %d : %lf\n", v,SINK,e->getCost(v));
		}
	}
	
	for(int i = 0 ; i < adj[poke].size() ; i++){
		Edge * e = adj[poke][i];
		int v = e->other(poke);
		if(v < n && e->residualTo(v) == 0){
			//printf("%d -> %d : %lf\n", poke,v,e->getCost(v));
			cost -= e->getCost(v);
		}
	}
	for(int i = 0 ; i < adj[ultra].size() ; i++){
		Edge * e = adj[ultra][i];
		int v = e->other(ultra);
		if(v < n && e->residualTo(v) == 0){
			//printf("%d -> %d : %lf\n", ultra,v,e->getCost(v));
			cost -= e->getCost(v);
		}
	}

	//printf("mf = %d\n", mf);	
	return cost;
}


void data1();

int main(){
	 data1();
}

void data1(){
	


	int a,b;
	cin >> n >> a >> b;
	adj.assign(n + 100, edgeList());
	vector<double> p(n,0), u(n,0);	
	for(int i = 0 ; i < n ; i++){
		cin >> p[i];
	}
	for(int i = 0 ; i < n ; i++){
		cin >> u[i];
	}
	
	SRC  = n;
	SINK = n+1;
	poke = n + 2;
	ultra = n + 3;

//	printf("SRC = %d\n", SRC);
//	printf("SINK = %d\n", SINK);
	addEdge(SRC, poke, a, 0);
	addEdge(SRC, ultra, b, 0);

	for(int i =  0 ; i < n ; i++){
		addEdge(poke, i, 1, -p[i]);
		addEdge(ultra, i, 1, -u[i]);
		addEdge(i, SINK, 1,0 );
		addEdge(i, SINK, 1, p[i] * u[i] );
	}

	double cost = maxFlow();
	cout << fixed << setprecision(8)<< cost << endl;
}


// NOTES

/*
 * Minimum cost flow problem
 *
 * minimize sum of ( Cij*Xij )
 *
 * Xij = units (flow) node_i to node_j
 * Cij = cost per unit of flow
 */
