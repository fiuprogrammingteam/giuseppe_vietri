#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<ll> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//
typedef pair<ii,int> Event;

int n,m;
vi a,b;
vector<Event> events;

int main(){
	FASTER;
	cin >> n;
	a.assign(n,0);
	b.assign(n,0);

	for(int i = 0 ; i < n ; i++){
		cin >> a[i];
	}
	cin >> m;
	for(int i = 0 ; i < m ; i++){
		int l,r,d;
		cin >> l >> r >> d;
		l--,r--;

		events.push_back(Event(ii(l,0), d));
		events.push_back(Event(ii(r,2), d));
	}

	for(int i = 0 ; i < n ; i++){
		events.push_back(Event(ii(i,1), 0));
	}

	sort(events.begin(), events.end());

	ll sum = 0;
	for (int i = 0; i < events.size(); ++i) {
		Event e = events[i];

		int j = e.first.first;
		int t = e.first.second ;
		int v = e.second;

		if(t == 0){
			sum += v;
		}else if(t == 2){
			sum -= v;
		}else{
			b[j] = a[j] + sum;
			printf("b[%d] = %lld %lld = %lld", j, a[j], sum, b[j]);
		}
		printf("sum = %lld\n", sum);
	}


	vi peak;
	int last_peak = 0;
	int ans = 0;
	for (int i = 0; i < n; ++i) {
		if(i == 0 || i == n-1 || (b[i-1]> b[i] && b[i+1]> b[i])){
			ans = max(ans, i - last_peak + 1);
			last_peak = i;
		}
	}

	for (int i = 0; i < n; ++i) {
		printf("%lld ", a[i]);
	}
		printf("\n");
	for (int i = 0; i < n; ++i) {
		printf("%lld ", b[i]);
	}
	printf("\n");

	printf("%d\n", ans);

	return 0;
}
