#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int main(){
	FASTER;
	ll n,a,b,c;
	cin >> n >> a >> b >> c;
	if(n % 4 == 0){
		cout << 0 << endl;
		return 0;
	}

	ll C[4] = {0,a,b,c};
	ll ans = 2e15;

	for (int i = 0; i < 4; ++i) {
		for (int j = 0; j < 4; ++j) {
			for (int k = 0; k < 4; ++k) {
				for (int z = 0; z < 4; ++z) {
					ll cost = C[i] + C[j] + C[k] + C[z];
//					printf("%lld cost = %lld\n",n + i + j + k + z,cost);
					if((n + i + j + k + z) % 4 == 0){
						ans = min(ans,cost);
					}
				}
			}
		}
	}


	cout << ans << endl;

	return 0;
}
