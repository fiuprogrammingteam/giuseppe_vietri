#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int n, d;

int M[12] = {
		31, // jan
		28,  // feb
		31, 	// march
		30,		// apri
		31,		// may
		30,		// jun
		31,		// jul
		31, 	// ago
		30,		// sep
		31,		// oct
		30,		// nov
		31,		//dec
	};


int main(){
	FASTER;


	cin >> n >> d;
	n--;
	d--;

	int col = 0;
	col ++;
	for (int i = 0; i < M[n]; ++i) {
//		printf("i = %d:\td = %d\n",i,d);

		if(i != 0 &&  d == 0) col++;

		d = (d + 1) % 7;
	}

	cout << col << endl;



	return 0;
}
