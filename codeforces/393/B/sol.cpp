#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//


int n,m,k;


ll sum(ll h){

	return h * (h+1)/2;

}

ll min_need(ll h){


//	printf("h = %lld\n",h);

	ll ret = sum(h-1) * 2;

//	printf("ret = %lld\n",ret);

	int l = max(0ll,	h-1 - k);
	int r = max(0ll, k + h - n);


//	printf("l = %d r = %d\n",l,r);
//	printf("ret = %lld\n",ret);

	ret -= sum(l);
	ret -= sum(r);
//	printf("ret = %lld\n",ret);


	ret += max(0ll, k - (h-1));
	ret += max(0ll, n - (k + h));
//	printf("ret = %lld\n",ret);

	return ret + h;

}

int main(){
	FASTER;
	cin >> n >>m >> k;
	k--;
//	printf("m = %lld\n", min_need(4));
//	return 0;



	ll lo = 1,
			hi = 2e9,
			h = 0,
			ans = 0;


	for (int i = 0; i < 500; ++i) {

		h = (lo + hi) >> 1;

		ll need = min_need(h);

//		printf("lo = %lld, hi = %lld\n",lo,hi);
//		printf("h = %lld need = %lld\n",h,need);

		if(need <= m){
			lo = h+1;
			ans = h;
		}else{
			hi = h;
		}
	}


	cout << ans << endl;
	return 0;
}
