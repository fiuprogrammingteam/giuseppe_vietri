#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//
#define MAXN (200000+10)
int n;
int P[MAXN];
int B[MAXN];

int vis[MAXN][2];


void dfs(int u, int b){

	vis[u][b] = 1;

//	printf("u = %d b = %d\n",u+1,b);
	int nb;

	if(B[u]){
		nb = 1 - b;
	}else{
		nb = b;
	}

	if(!vis[P[u]][nb]){
		dfs(P[u],nb);
	}
}

int main(){
	FASTER;
	cin >> n;

	for (int i = 0; i < n; ++i) {
		cin >> P[i];
		P[i]--;
	}

	for (int i = 0; i < n; ++i) {
		cin >> B[i];
	}

	MEM(vis,0);
	int cc = 0;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < 2; ++j) {
			if(!vis[i][j]){
//				printf("i = %d\n",i+1);
				dfs(i,j);
				cc++;
			}
		}
	}

	cout << cc -1 	<< endl;

	return 0;
}
