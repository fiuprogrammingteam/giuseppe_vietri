#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int main(){
	FASTER;
	int n;
	string s;
	cin >> n;
	cin >> s;

	for(int i = 0 ; i < n ; i++){
		
		if(s.substr(i,3) == "ogo"){
			int j = i + 3;
			while(s.substr(j, 2) == "go")j+=2;

			for(int k = i ; k < i+3 ; k++)
				s[k] = '*';
			s = s.substr(0,i+3) + s.substr(j);
			i = i + 2;
		}
		if(i>=s.size())break;
	}

	cout << s << endl;

	return 0;
}
