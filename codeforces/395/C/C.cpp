#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)
#define ALL(v)v.begin(),v.end()
#define PB(v)push_back(v)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//
#define MAXN 300000

int n;
int C[MAXN];
vector<vi> G;
int P[MAXN];
int Sz[MAXN];
int find(int u){
	if(P[u]<0)return u;
	return P[u] = find(P[u]);
}

bool conn(int u, int v){
	return find(u) == find(v);
}

void join(int u , int v){

	if(conn(u,v))return;

	u = find(u);
	v = find(v);

	if(P[u] < P[v]){
		P[v] = u;
		Sz[u] += Sz[v];
	}else if(P[v] < P[u]){
		P[u] = v;

		Sz[v] += Sz[u];
	}else{
		P[v] = u;
		P[u]--;
		Sz[u] += Sz[v];
	}
}

void dfs1(int u, int p = -1){

	for(auto v : G[u]){
		if(v!=p){
			if(C[u] == C[v])join(u,v);
			dfs1(v,u);
		}
	}
}


int vertex=-1;
int counted[MAXN];

void dfs2(int u, int p = -1){


	int m = 0;

	int group = find(u);
	m += Sz[group];
	counted[group] = u;

	for(auto v : G[u]){

		group = find(v);
		if(counted[group] != u) {
			m += Sz[group];
			counted[group] = u;
		}
	}

	if(m == n){
		vertex = u;
	}

	for(auto v : G[u]){
		if(v!=p){
			dfs2(v,u);
		}
	}
}


int main(){
	FASTER;

	cin >> n;
	G.assign(n+1,vi());

	for (int i = 0; i < n-1; ++i) {
		int u,v;
		cin >>u >> v;
		u--,v--;
		G[u].PB(v);
		G[v].PB(u);
	}

	MEM(P,-1);
	MEM(Sz,0);
	MEM(counted,-1);

	for (int i = 0; i < n; ++i) {
		cin >> C[i];
		Sz[i]++;
	}




	dfs1(0);

	dfs2(0);




	if(vertex == -1){
		cout << "NO\n";
	}else{
		cout << "YES\n";

		cout << vertex + 1<< endl;
	}

	return 0;
}
