#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)
#define ALL(v)v.begin(),v.end()
#define PB(v)push_back(v)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//
#define MAXN 300000

struct rect{
	int lx, ly, ux, uy;
};

struct event{
	int type, x, y1, y2, id;
	bool operator < (event a) const{
		return this->x < a.x;
	}
};
struct event_h{
	int type, y, x1, x2, id;
	bool operator < (event_h a) const{
		return this->y < a.y;
	}
};
int n;
vector<rect > R;
vector<event> E;
vector<event_h> Eh;

set<ii> conn;

vector<vi> G;

void addEdge(int u, int v){
	if(u == v)return;
	if(!conn.count(ii(u,v))){

		conn.insert(ii(u,v));
		conn.insert(ii(v,u));
		G[u].PB(v);
		G[v].PB(u);

		printf("add edge: %d %d\n", u,v);

	}

}

int Col[MAXN];
int vis[MAXN];
int dp[MAXN][5];

bool dfs(int u, int col){

	vis[u] = 1;
	Col[u] = col;


	int &ret = dp[u][col];


	if(ret == -1){
		ret = 0;

		for(auto v : G[u]){

			bool thisok = false;
			if(vis[v]){
				if(col == Col[v])return ret = false;
				continue;
			}

			for (int j = 1; j <= 4; ++j) {
				if(j == col)continue;
				int tmp = dfs(v,j);
				ret = ret && tmp;
			}
		}
	}

	return ret;
}

int main(){
	FASTER;
	cin >> n;


	G.assign(n+1,vi());
	MEM(Col,0);
	MEM(vis,0);

	for (int i = 0; i < n; ++i) {
		rect r;
		cin >> r.lx >> r.ly >> r.ux >> r.uy ;
		R.PB(r);

		// Start segment
		event  e1 = {2, r.lx, r.ly, r.uy, i};
		// End segment
		event  e2 = {1, r.ux, r.ly, r.uy, i};

		E.PB(e1);
		E.PB(e2);

		// Start segment
		event_h eh1 = {2, r.ly, r.ly, r.uy, i};
		// End segment
		event_h eh2 = {1, r.uy, r.ly, r.uy,i};

		Eh.PB(eh1);
		Eh.PB(eh2);
	}

	sort(ALL(E));
	sort(ALL(Eh));



	map<int,set<int> > Y;

	for (int i = 0; i < E.size(); ++i) {

		event e = E[i];

		for(auto it = Y[e.y1].begin(); it != Y[e.y1].end() ; ++it){
			addEdge(*it, e.id);
		}
		for(auto it = Y[e.y2].begin(); it != Y[e.y2].end() ; ++it){
			addEdge(*it, e.id);
		}

		// start
		if(e.type == 2){

			Y[e.y1].insert(e.id);
			Y[e.y2].insert(e.id);
		}else{

			Y[e.y1].erase(e.id);
			Y[e.y2].erase(e.id);
		}
	}


	map<int,set<int> > X;

	for (int i = 0; i < Eh.size(); ++i) {

		event_h e = Eh[i];

		for(auto it = X[e.x1].begin(); it != X[e.x1].end() ; ++it){
			addEdge(*it, e.id);
		}
		for(auto it = X[e.x2].begin(); it != X[e.x2].end() ; ++it){
			addEdge(*it, e.id);
		}

		// start
		if(e.type == 2){

			X[e.x1].insert(e.id);
			X[e.x2].insert(e.id);
		}else{

			X[e.x1].erase(e.id);
			X[e.x2].erase(e.id);
		}
	}

	MEM(Col,0);
	MEM(vis,0);

	MEM(dp, -1);
	for (int i = 1; i <= 4; ++i) {
		if(dfs(0,i))break;

	}

	cout << "YES\n";
	for (int i = 0; i < n; ++i) {
		cout << Col[i] << endl;
	}
	return 0;
}
