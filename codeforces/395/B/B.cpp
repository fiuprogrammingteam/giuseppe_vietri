#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)
#define ALL(v)v.begin(),v.end()
#define PB(v)push_back(v)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//
#define MAXN 300000

int A[MAXN], B[MAXN];

int main(){
	FASTER;
	int n;
	cin >> n;

	for (int i = 0; i < n; ++i) {
		cin >> A[i];
	}

	for (int i = 0; i < n; i += 2) {

		int a = A[i];
		int b = A[n-i-1];
		if(i>=n-i-1)break;

		A[i] = b;
		A[n-i-1] = a;
	}

	for (int i = 0; i < n; ++i) {
		cout << A[i] << " ";
	}
	cout << endl;

	return 0;
}
