#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)
#define ALL(v)v.begin(),v.end()

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int A[200], B[200];
int T[200];
int main(){
	FASTER;


	int n,L;
	cin >> n >> L;

	MEM(A,0);
	MEM(B,0);

	MEM(T,0);
	for (int i = 0; i < n; ++i) {
		cin >> A[i];

		T[A[i]]=1;
	}

	for (int i = 0; i < n; ++i) {
		cin >> B[i];
	}

	for (int i = 0; i < L; ++i) {

		int p = i;
		bool ok = true;
		for (int j = 0; j < n && ok; ++j) {

			p = i + B[j];
			p %= L;
			if(T[p] == 0)ok = false;
		}

		if(ok){
			cout << "YES\n";
			return 0;
		}
	}

	cout << "NO\n";
	return 0;
}
