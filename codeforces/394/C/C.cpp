#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)
#define ALL(v)v.begin(),v.end()

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//


vector<string> G;
int R,C;

int lower(char c){

	if(c >='a' && c <= 'z')
		return 4;
	return 0;
}
int digit(char c){

	if(c >='0' && c <= '9')
		return 2;
	return 0;
}

int special(char c){

	if(c == '#' || c == '*' || c == '&')
		return 1;
	return 0;
}

int dp[100][10];


int solve(int r , int req){

	int &ret = dp[r][req];


	if(r == R){

		if(req == (1<<3)-1)
			ret = 0;
		else
			ret = 1e9;
	}


	if(ret == -1){
		ret = 1e9;

		for (int i = 0; i < C; ++i) {
			char c = G[r][i];
			int nreq = req | lower(c) | digit(c) | special(c);

			int temp = solve(r+1, nreq) + min(i,C-i);

			ret = min(ret, temp);

		}

	}

	return ret;
}

int main(){
	FASTER;
	cin >> R >> C;

	for (int i = 0; i < R; ++i) {
		string s;
		cin >> s;
		G.push_back(s);
	}

	MEM(dp,-1);

	cout << solve(0,0) << endl;



	return 0;
}
