#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)
#define ALL(v)v.begin(),v.end()

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

ll A[200000];
ll P[200000];
ll B[200000];

int main(){
	FASTER;

	int n,l,r;
	cin >> n >> l >> r;

	for (int i = 0; i < n; ++i) {
		cin >> A[i];
	}
	for (int i = 0; i < n; ++i) {
		cin >> P[i];
	}
	for (int i = 0; i < n; ++i) {
		B[i] = A[i] + P[i];
	}

	ll maxi = 0, mini = 2e17;

	for (int i = 0; i < n; ++i) {
		B[i] = A[i] + P[i];
		maxi = max(maxi, B[i]);
		mini = min(mini, B[i]);
	}


	ll diff = (maxi < r) ? 0 : maxi - r;
//	printf("min = %lld max = %lld diff = %d\n" , mini, maxi,diff);

	if(mini - diff < l )cout << -1 << endl;
	else{

		for (int i = 0; i < n; ++i) {
			cout << B[i] - diff  << " ";
		}
		cout << endl;
	}


	return 0;
}
