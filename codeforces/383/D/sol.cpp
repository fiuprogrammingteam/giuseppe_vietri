#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

ll dp[1010][1010][2];

struct hos{

	int u, w, b;

};

typedef vector<hos> hos_v;

int w[1010];
ll b[1010];
int group[1010];
int group_sz[1010];

ll group_w[1010];
ll group_b[1010];

int vis[1010];
hos_v hoses;

vector<vi> G;
int N,M,W;

int set_group(int u, int group_id){
	group[u]  = group_id;
	vis[u] = 1;
	int cnt = 1;

	group_w[group_id] += hoses[u].w;
	group_b[group_id] += hoses[u].b;

	for (auto v : G[u]) {
			if(!vis[v]){
				cnt += set_group(v, group_id);
			}
	}
	return cnt;
}

bool cmp(hos u, hos v){
	if(group[u.u] == group[v.u])return u.u < v.u;
	return group[u.u] < group[v.u];
}

ll sol(int i, int w_left, int tak){

	if(w_left<0){
		return -1e9;
	}

	ll &ret = dp[i][w_left][tak];

	if(i == N){
		ret = 0;
	}

	if(ret == -1){
		ret = 0;
		int u = hoses[i].u;

		int gid = group[u];
		bool isfirst = (i == 0) || group[hoses[i-1].u] != gid;
		bool islast = (i + 1 == N) || group[hoses[i+1].u] != gid;

		if(isfirst){
			ll tmp = sol(i+group_sz[gid], w_left - group_w[gid], 0) + group_b[gid];
			ret = max(ret,tmp);
		}

		ll op;
		if(!tak){
			op = sol(i+1, w_left - hoses[i].w, !islast) + hoses[i].b;
			ret = max(ret,op);
		}



		op = sol(i+1, w_left, !islast && tak);
		ret = max(ret,op);

	}

	return ret;
}

int main(){
	FASTER;

	cin >> N >> M >> W;

	for (int i = 0; i < N; i++) {

		hos h = {i,0,0};
		hoses.push_back(h);

	}


	for (size_t i = 0; i < N; i++) {

		cin >> hoses[i].w;
	}

	for (size_t i = 0; i < N; i++) {
		cin >> hoses[i].b;
	}

	G.assign(N,vi());

	for (size_t i = 0; i < M; i++) {
		int u,v;
		cin >> u >> v;
		u--,v--;
		// printf("%d %d\n",u,v);
		G[u].push_back(v);
		G[v].push_back(u);
	}

	MEM(group, 0);
	MEM(vis, 0);
	int id = 0;
	for (int i = 0; i < N; i++) {
			if(!vis[i]){
				++id;
				group_sz[id] = set_group(i, id);
			}
	}

	sort(hoses.begin(), hoses.end(), cmp);

	//printf("u\tw\tb\tgid\tg_sz\tg_w\tg_b\n");

	for (size_t i = 0; i < N; i++) {
		int u = hoses[i].u;
		int w = hoses[i].w;
		int b = hoses[i].b;
		int gid = group[u];
		//printf("%d\t%d\t%d\t%d\t%d\t%d\t%d\n",u,w,b,gid,group_sz[gid],group_w[gid],group_b[gid]);
	}
	MEM(dp,-1);
	ll ans = sol(0, W,0);


	printf("%lld\n",ans);

	return 0;
}
