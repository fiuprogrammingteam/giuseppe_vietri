#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int C[200];
int vis[200];
int loop_sz[200];
int n;

int cnt_loop(int src, int i){
	if(i == src)return 0;

	if(vis[i])return -1e9;
	vis[i] = 1;

	return cnt_loop(src, C[i]) + 1;
}

int main(){
	FASTER;
	scanf("%d", &n);

	for (size_t i = 0; i < n; i++) {
		scanf("%d", C + i);
		C[i]--;
	}

	ll ans = 1;
	for (size_t i = 0; i < n; i++) {
		MEM(vis,0);
		vis[i]=1;
		loop_sz[i] = cnt_loop(i,C[i]) + 1;

		if(loop_sz[i] < 0){
			printf("-1\n");
			return 0;
		}

		// printf("i=%d: sz = %d\n",(int)i, loop_sz[i]);

		if(loop_sz[i] % 2 == 0){
			ans = lcm(ans, 1ll * loop_sz[i]/2);
		}else{
			ans = lcm(ans, 1ll *  loop_sz[i]);
		}
	}

	printf("%lld\n",ans);
	return 0;
}
