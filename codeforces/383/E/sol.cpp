#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int n;
int F[200002];
int vis[200002];
ii bg[200002];

vector<vi> g;

void dfs(int u, int d){
	F[u] = d;
	vis[u] = 1;

	for(auto v: g[u]){
		if(vis[v])continue;
		dfs(v,d^1);
	}
}

int main(){
	FASTER;
	cin >> n;
	g.assign(2*n+20,vi());
	for(int i = 0 ; i < n ; i++){
			int u,v;
			cin >> u >>v;
			bg[i] = ii(u,v);
			g[u].push_back(v);
			g[v].push_back(u);
	}

	for (size_t i = 1; i <= n; i++) {
		g[i*2-1].push_back(2*i);
		g[i*2].push_back(2*i-1);
	}

	MEM(vis,0);
	for (size_t i = 1; i <= 2*n; i++) {
		/* code */
		if(!vis[i])dfs(i,0);
	}

	for (size_t i = 0; i < n; i++) {
		printf("%d %d\n", F[bg[i].first] + 1, F[bg[i].second]+1);
	}



	return 0;
}
