#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int main(){
	FASTER;
	ll c = 1378;
	ll n = c;

	cin >> n;


	if(n == 0){
		printf("1\n");
		return 0;
	}

	n--;
	n = n % 4;

	switch(n){
		case 0:
			printf("8");
			break;
		case 1:
			printf("4");
			break;
		case 2:
			printf("2");
			break;
		case 3:
			printf("6");
			break;
	}

	printf("\n");


	// for (size_t i = 0; i < 20; i++) {
	// 	/* code */
	//
	// 	cout << n << endl;
	// 	n *= c;
	// }



	return 0;
}
