#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int A[200];
int main(){
	FASTER;
	int n;
	cin >> n;



	int mx = 0;

	for (int i = 0; i < n; ++i) {
		cin >> A[i];
		mx = max(mx,A[i]);
	}

	int ans = 0;

	for (int i = 0; i < n; ++i) {
		ans += mx - A[i];
	}
	cout << ans << endl;

	return 0;
}
