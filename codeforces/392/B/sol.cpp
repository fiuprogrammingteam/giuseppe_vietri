#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int mp(char c){

	if(c == 'R')return 0;
	if(c == 'B')return 1;
	if(c == 'Y')return 2;
	if(c == 'G')return 3;
	return -1;
}

int pos(int b){
	int p = 0;

	for (int i = 0; i < 4; ++i) {
		if((b&1) == 0)return p;
		p++;
		b>>=1;
	}
	return -1;
}

int main(){
	FASTER;

	string s;
	cin >> s;

	int V[4];
	int Cnt[4];

	MEM(V,0);
	MEM(Cnt,0);


	vi A;

	for (int i = 0; i < s.size(); ++i) {
		A.push_back(mp(s[i]));
	}





	for (int i = 0; i < 100; ++i) {

//		if(A[0])
//			V[A[0]]=1,seen++;
//		if(A[1])
//			V[A[1]]=1,seen++;
//		if(A[2])
//			V[A[2]]=1,seen++;
//		if(A[3])
//			V[A[3]]=1,seen++;


//		printf("i=%d\n",i);
		for (int j = 0; j < A.size(); ++j) {

			int seen = 0;
			for (int k = max(0,j-3); k < min((int)A.size(), j+4); ++k) {
				if(A[k] != -1){
					seen |= (1 << A[k]);
				}
			}

//			printf("j = %d: seen = %d\n", j, seen);

			if(__builtin_popcount(seen) == 3){

				int p = pos(seen);

//				printf("\tp = %d\n",p);

				if(A[j] == -1){
					Cnt[p]++;
					A[j] = p;
				}
			}
		}
	}


//	for (int i = 0; i < A.size(); ++i) {
//		cout << A[i] << " ";
//	}
//	cout << endl;

	for (int i = 0; i < 4; ++i) {
		cout << Cnt[i] << " ";
	}
	cout << endl;

	return 0;
}
