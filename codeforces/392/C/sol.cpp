#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int G1[200][200];
int G2[200][200];
int C[200][200];

int main(){
	FASTER;

	int n,m,x,y;
	ll k;
	cin >> n >> m >> k >> x >> y;

	int id = 0;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {
			G1[i][j] = id++;
		}
	}
	id = 0;
	for (int i = n-1; i>=0; --i) {
		for (int j = 0; j < m; ++j) {
			G2[i][j] = id++;
		}
	}

	int S = n * m;

	ll mn = 1e18;
	ll mx = -1e18;
	ll ans = 0;

	ll c = 2 * n * m;
	printf("c = %lld\n",c);
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j) {

			int s1,s2,s3,s4;

			s1 = j;

			if(i>0) s1 += m +  2*(m * (i-1));

			s3 = m - j;

			if(i<n-1) s3 += m + 2*(n-i-2)*m;



			c = s1 + s3;

			ll kp = k - 1ll*G1[i][j]-1;

			ll q = 2 * ceil( 1.0 * kp /c);

			if(q == 0)q = 1;
			else if(q<0)q = 0;
			else{
				if(kp % c < s3)q--;
			}

			if(i+1 == x && j+1 == y)ans = q;

			printf("%03lld,%lld,%d\t",q,c,s3);
//			printf("kp = %lld\n",kp);
			mn = min(mn, q);
			mx = max(mx, q);

		}
		printf("\n");
	}

	cout << mx << " " << mn << " " << ans << endl;

	return 0;
}
