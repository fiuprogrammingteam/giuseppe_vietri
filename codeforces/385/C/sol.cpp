#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int n,m, k;
int V;
vi vis;
vi cc;
vi K;
vector<vi> G;

int dfs(int u){
//	printf("u = %d\n",u);
	vis[u] = 1;
	V++;
	int cnt = 1;
	for (int i = 0; i < G[u].size(); ++i) {
		int v = G[u][i];
		if(vis[v] == 0){
			cnt += dfs(v);
		}
	}
	return cnt;
}

int main(){
	FASTER;
	cin >> n >> m >> k;


	G.assign(n+1, vi());
	cc.assign(k,0);
	vis.assign(n+1,0);

	for (int i = 0; i < k; ++i) {
		int x;
		cin >> x;
		K.push_back(x);
		int cnt = dfs(x);

	}

	V = 0;

	for (int i = 0; i < m; ++i) {
		int u,v;
		cin >> u >> v;

		G[u].push_back(v);
		G[v].push_back(u);
	}

	for (int i = 0; i < k; ++i) {

		cc[i] = dfs(K[i]);
	}
	sort(cc.rbegin(), cc.rend());
	cc[0] += n - V;


	ll ans = 0;


	for (int i = 0; i < k; ++i) {
//		printf("cc[%d] = %d\n", i, cc[i]);
		ans += 1ll * cc[i] *( cc[i] - 1) / 2	;
	}

	cout << ans - m << endl;
	return 0;
}
