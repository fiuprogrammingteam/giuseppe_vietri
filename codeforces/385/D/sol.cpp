#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int main(){
	FASTER;
	vi ans;
	int n;


	queue<ii> q;

	cin >> n;

	vi ind;
	for (int i = 1; i <= n; ++i) {
		q.push(i);
	}

	q.push(ii(1,n));


	for (int i = 0; i <= 20; ++i) {

		ii u = q.front();
		q.pop();

		int l = u.first,  r = u.second;

		int m = (r + l) / 2;


		if(r>l){
			q.push(ii(l,m));
			q.push(ii(m+1,r));
		}

		if(i == 0)continue;

		int k = r - l + 1;
		cout << k << endl;
		for (int i = l; i <= r; ++i) {
			cout << i << " ";
		}
		cout << endl;

		fflush(strout);

		for (int i = 0; i < n; ++i) {
			int x;
			cin >> x;
			ans[i] = min(ans[i],x);
		}
	}

	return 0;
}
