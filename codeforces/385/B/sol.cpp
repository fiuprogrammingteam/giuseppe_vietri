#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int n,m;
vector<string> G;



int main(){
	FASTER;
	cin >> n >> m;

	int min_col, max_col, min_row, max_row;

	min_col = min_row = 100000000;
	max_col = max_row = -100000000;

	for (int i = 0; i < n; ++i) {
		string s;
		cin >> s;
		G.push_back(s);

//		cout << s << endl;
		for (int j = 0; j < m; ++j) {
			if(G[i][j] == 'X'){
				min_col = min(min_col, j);
				max_col = max(max_col, j);
				min_row = min(min_row, i);
				max_row = max(max_row, i);

			}
		}

	}

	vector<string> tmp;
//	printf("%d %d %d %d\n",min_col, max_col, min_row, max_row);
	for (int i = 0; i < n; ++i) {

		if(i>= min_row && i <= max_row){
			int l = max_col - min_col+1;
			string q = G[i].substr(min_col,l);
			tmp.push_back(q);
//			cout << q << endl;
		}
	}

	G = tmp;
	n = G.size();
	m = G[0].size();




	bool YES = true;


	for (int j = 0; j < n; ++j) {
		for (int k = 0; k < m; ++k) {
			if(G[j][k] == '.')
				YES = false;
		}
	}


	if(YES){
		cout << "YES\n";
	}else{
		cout << "NO\n";
	}

	return 0;
}
