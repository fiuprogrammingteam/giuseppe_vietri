#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//
#define max_int 1000000000
int main(){
	FASTER;

	ll a,b;
	cin >> b;

	a = 2;

	for (ll x = 1; x <= 100000; ++x) {
		for (ll y = 1; y <= 10000; ++y) {
			if(x == y)continue;

			ll n1 = -(x*y)*(x*y);
			ll n2 = b * (x + y) - a* (x*y);

			if(n2 == 0)continue;

			if(n1 % n2 != 0)continue;

			ll n = n1 / n2;

			if((n * b) % ( x * y) != 0)continue;

			ll z = (n * b) / (x * y);

			if(z <= 0)continue;

			if(x != z && y != z ){

				if(z > max_int)continue;


				cout << x <<" " << y << " " << z << endl;

				return 0;
			}

		}
	}

	cout << -1 << endl;

	return 0;
}
