#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int main(){
	FASTER;
	ll n,k;
	cin >> n >> k;


	ll p = 1;
	ll p2 = 1;

	int cnt = 0;

	while(1){

//		printf("%lld %lld %lld\n", p,p2,k);

		if(p2 == k)break;

		if(p2 * 2 > k){
			k-= p2;
			p2 = 1;
			p = 1;
		}else if(p2 < k){
			p2 *= 2;
			p++;
		}
	}

	cout << p << endl;

	return 0;
}
