#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int main(){
	FASTER;

	int n,a,b;

	cin >> n >> a >> b;

	string s;
	cin >> s;

	a--,b--;

	if(s[a] == s[b]){
		cout << 0 << endl;
		return 0;
	}


	int j = a;

	for (int i = 0; i < n; ++i) {

		if(s[i] == s[a]){

			if(abs(b - i) < abs(b - j)){
				j = i;
			}

		}
	}

	int ans1 = abs(b - j);

	j = b;

	for (int i = 0; i < n; ++i) {

		if(s[i] != s[a]){

			if(abs(a - i) < abs(a - j)){
				j = i;
			}
		}
	}

	int ans2 = abs(a - j);


	cout << min(ans1,ans2) << endl;



	return 0;
}
