echo "Creating folder "$1

mkdir $1
touch "$1/$1.cpp"
touch "$1/in1"
touch "$1/in2"
touch "$1/out1"
touch "$1/out2"
touch "$1/Makefile"

echo "solmake: $1.cpp\n\tg++ -g -std=c++11 $1.cpp

test: solmake\n\t./a.out < in1" > "./$1/Makefile" 

#cp ~/Code/giuseppe_vietri/Makefile  "./$1/Makefile"
cat ~/Code/giuseppe_vietri/template.cpp > "./$1/$1.cpp"


