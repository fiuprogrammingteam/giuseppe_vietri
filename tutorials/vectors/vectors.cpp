#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)
#define ALL(v)v.begin(),v.end()
#define PB(v)push_back(v)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int main(){
	FASTER;

    vector<int> A; // integer vector
    vector<double> B; // double vector
    vector<vector<int> > C; // vector for vectors
    vector<set<int> > D; // vector of sets

    ////////////////////////////////////////////
    // using the macro
    vi E; // vector of integers short cut
    vii F; // vector of pairs
    
    ii p(2,3);

    cout << p.first << p.second;


    /////////////////////////////////////////////////
    // operations
    vi G;

    // Add
    G.push_back(2);

    // access
    cout << G[0];

    // binary search
    vi::iterator it = lower_bound(G.begin(), G.end(), 2);
    vi::iterator it = lower_bound(ALL(G), 2);


    int tmp = *it;

    // clear vector
    G.clear();


	return 0;
}
