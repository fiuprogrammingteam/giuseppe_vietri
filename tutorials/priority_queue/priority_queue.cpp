#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)
#define ALL(v)v.begin(),v.end()
#define PB(v)push_back(v)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

class cmp{
    public :
        bool operator ()(int a, int b){
            return a>b;
        }
};

int main(){
	FASTER;

    priority_queue<int> q;

    q.push(2);
    q.push(6);
    q.push(-3);

    // output 6 2 -3
    while(!q.empty()){
        cout << q.top() << " ";
        q.pop();
    }
    cout << endl;

    // Store in reverse order
    priority_queue<int, vi, cmp> q1;
    q1.push(2);
    q1.push(6);
    q1.push(-3);
    // output -3 2 6
    while(!q1.empty()){
        cout << q1.top() << " ";
        q1.pop();
    }
    cout <<endl;



	return 0;
}
