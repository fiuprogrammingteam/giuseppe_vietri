#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)
#define ALL(v)v.begin(),v.end()
#define PB(v)push_back(v)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int main(){
	FASTER;

    map<int,int> M1;

    ///////////////////////////////////
    // operators
    M1[2] = 3;
    M1[8] = -1;
    M1[3] = 2;
    M1[3] = 1;

    cout << M1[2] << endl; // 3
    cout << M1[9] << endl; // 0
    cout << M1[3] << endl; // 1

    cout << M1.count(8) << endl; // 1  - test whether element is in the map

    // remove a key

    map<int,int>::iterator it;

    it= M1.find(3);
    M1.erase (it);                   // erasing by iterator

    M1.erase (8);                  // erasing by key

    it=M1.find ('e');
    M1.erase ( it, M1.end() );    // erasing by range
    
    ////////////////////////////
    // iterate

    cout << "key\tvalue\n";
    for(map<int,int>::iterator j = M1.begin() ; j != M1.end(); j++){



        cout << j->first << "\t" << j->second << endl;
    }

	return 0;
}
