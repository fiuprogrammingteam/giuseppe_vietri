#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)
#define ALL(v)v.begin(),v.end()
#define PB(v)push_back(v)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

void inc(int &p){
    p = p + 1;
}

void inc_wrong(int p){
    p = p + 1;
}


int main(){
	FASTER;


    int * a;
    int * b;


    int x,y;

    x = 3;
    a = &x;

    *a = 2;

    cout << *a << endl;

    x = 7;

    cout << *a << endl;


    ///////////////////////////////////////////////////////////
    
    inc(x);
    cout << "x = " << x << endl;

    inc_wrong(x);
    cout << "x = " << x << endl;

	return 0;
}
