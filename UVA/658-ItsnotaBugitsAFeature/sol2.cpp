#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<ll,ll> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//
int n,m;
vi Bn,Bp,Fn,Fp;
vector<ll> sec;
//bitmask<1100000> dis
void deb(int b){
	
	for(int i = 0 ; i < 20 ; i++){
		printf("%d", b&1);
		b>>=1;
	}
	printf("\n");
}

vector<string> B;
vector<string> F;

bool check(int j, int b){
	for(int i = 0 ; i < n ; i++){
		if(B[j][i] == '+' && !(1&(b >> i)))return false;
		if(B[j][i] == '-' &&  (1&(b >> i)))return false;
	}
	return true;
}

int patch(int j , int b){
	for(int i = 0 ; i < n ; i++){
		if(F[j][i] == '+')b |= (1 << i);
		if(F[j][i] == '-' && b&(1<<i) )b -= (1 << i);
	}
	return b;
}

int main(){
	FASTER;

	int Prod = 1;
	while(cin >> n >> m,n||m){
		if(Prod > 1)printf("\n");
		B.clear();
		F.clear();
		for(int i = 0 ; i < m ; i++){
			ll s;
			string b,f;
			cin >> s >> b >> f;

			sec.push_back(s);

			reverse(b.begin(), b.end());
			reverse(f.begin(), f.end());
			B.push_back(b);
			F.push_back(f);
		}

		int src = (1 << n)-1;	
		priority_queue<ii> q;
		vector<ll> dist((1<< n) + 10, 1e18);
		
		q.push(ii(0,src));
		dist[src] = 0;

		while(!q.empty()){
			int u = q.top().second;
			ll d = -q.top().first;
			q.pop();
			
			if(d > dist[u])continue;
			if(u == 0)break;
			//deb(u);
			for(int i = 0 ; i < m ; i++){
				ll w = sec[i];	
				if(check(i, u)){
					int v = patch(i, u);
					if(dist[u] + w < dist[v]){
						dist[v] = dist[u] + w;
						q.push(ii(-dist[v], v));
					}
				}
			}
		}

		printf("Product %d\n", Prod++);
		//printf("%lld %lld\n",dist[0],(ll)(1e18)-2ll);
		if(dist[0] > (ll)(1e18)-2ll)
			printf("Bugs cannot be fixed.\n");
		else{
			printf("Fastest sequence takes %lld seconds.\n", dist[0]); 
		}
	}

	return 0;
}
