#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<ll,ll> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//
int n,m;
vi Bn,Bp,Fn,Fp;
vector<ll> sec;

void deb(int b){
	
	for(int i = 0 ; i < 20 ; i++){
		printf("%d", b&1);
		b>>=1;
	}
	printf("\n");
}

int main(){
	FASTER;

	int Prod = 1;
	while(cin >> n >> m,n||m){

		sec.clear();		
		Bp.clear();		
		Bn.clear();		
		Fp.clear();		
		Fn.clear();		

		for(int i = 0 ; i < m ; i++){
			ll s;
			string b,f;
			cin >> s >> b >> f;

			sec.push_back(s);

			reverse(b.begin(), b.end());
			reverse(f.begin(), f.end());

			int bp,bn,fp,fn;
			bp = bn = fp = fn = 0;

			for(int j = 0 ; j < b.size();j++){
				bp <<= 1;
				bn <<= 1;
				if(b[j] == '+') bp |= 1;
				if(b[j] == '-') bn |= 1;
			}

			for(int j = 0 ; j < f.size();j++){
				fp <<= 1;
				fn <<= 1;
				if(f[j] == '+') fp |= 1;
				if(f[j] == '-') fn |= 1;
			}

			Bp.push_back(bp);
			Bn.push_back(bn);
			Fp.push_back(fp);
			Fn.push_back(fn);
		}

		int src = (1 << n)-1;	
		priority_queue<ii> q;
		vector<ll> dist((1<< n) + 10, 1e18);
		
		q.push(ii(0,src));
		dist[src] = 0;

		while(!q.empty()){
			int u = q.top().second;
			ll d = -q.top().first;
			q.pop();
			
			if(d > dist[u])continue;
			if(u == 0)break;
			//deb(u);
			for(int i = 0 ; i < m ; i++){
				ll w = sec[i];	
				if( (u & Bp[i]) == Bp[i] && (u & Bn[i]) == 0){
					int v = u|Fp[i];
					v = v ^ (v& Fn[i]);

					if(dist[u] + w < dist[v]){
						dist[v] = dist[u] + w;
						q.push(ii(-dist[v], v));
					}
				}
			}
		}

		printf("Product %d\n", Prod++);
		//printf("%lld %lld\n",dist[0],(ll)(1e18)-2ll);
		if(dist[0] > (ll)(1e18)-2ll)
			printf("Bugs cannot be fixed.\n");
		else{
			printf("Fastest sequence takes %lld seconds.\n", dist[0]); 
		}
		printf("\n");
	}

	return 0;
}
