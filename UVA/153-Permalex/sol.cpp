#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int f[300];

ll per(int n){

    vi P;
    for(int i = 2 ; i <= n ; i++)P.push_back(i);
    
    for(int i = 'a' ; i <= 'z' ; i++){

        for(int j = 2 ; j <= f[i] ; j++){
            int tmp = j;
            for(int k = 0 ; k < P.size() ; k++){
                int g = gcd(tmp,P[k]);
                tmp/=g;
                P[k]/=g;
            }
        }
    }

    ll ans = 1;

    for(int i = 0 ; i < P.size() ; i++){
        ans *= P[i];
    }

    return ans;
} 

int main(){
	FASTER;
    
    string s;

    while(cin >> s, s != "#"){
        
        MEM(f,0);
        vi C;

        for(int i = 0 ;  i < s.size() ; i++){

            if(f[s[i]] == 0){
                C.push_back(s[i]);
            }

            f[s[i]]++;
        }

        sort(C.begin(),C.end());

        ll pos = 0;
        for(int i = 0 ; i < s.size(); i++){

            for(int j = 0 ; j < C.size(); j++){
                if(C[j] == s[i])break;
                f[C[j]]--;
                pos += per(s.size()-i-1);
                f[C[j]]++;
            }

            f[s[i]]--;
            //printf("i=%d\n",i);
            // Clean array
            for(int ch = 'a' ; ch <= 'z'; ch++){
                if(f[ch]==0){
                    //printf("ch[%c] = %d\n", ch, f[ch]);
                    vi::iterator it = find(C.begin(), C.end(), ch); 
                    if(it!=C.end())
                        C.erase(it);
                }
            }
        }

        cout.width(10);
        cout << pos + 1<<endl ;
    }

	return 0;
}
