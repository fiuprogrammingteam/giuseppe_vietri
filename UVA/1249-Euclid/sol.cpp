#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef pair<double,double> dd;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//
#define X first
#define Y second 
#define A 0 
#define B 1 
#define C 2
#define D 3
#define E 4
#define F 5
#define H 6
#define G 7

vector<dd> P;

double CAB;

double side_length(int p, int q){
	return hypot(P[p].X - P[q].X, P[p].Y - P[q].Y);
}

double area(double l1, double l2, double l3){
	double s = (l1+l2+l3)/2;
	return sqrt(s*(s-l1)*(s-l2)*(s-l3));
}

double para_area(){
	double b = side_length(A,B);
	double l = side_length(A,H); 
	double h = l * sin(CAB);
	return h*b;
}

void get_ang(){
	double a = side_length(A,C);
	double b = side_length(A,B);
	double c = side_length(B,C);
	CAB = acos((a*a + b*b - c*c)/(2*a*b));	
	//printf("a,b,c, = %lf %lf %lf: %lf\n", a,b,c,CAB);
}

int main(){
	FASTER;

	while(1){
		bool done = true;	
		P.assign(14,dd(0,0));
		for(int i = 0 ; i < 6 ; i++){
			cin >> P[i].X >> P[i].Y;
			if(fabs(P[i].X) > 1e-9  || fabs(P[i].Y) > 1e-9)done = false;
		}
		if(done )break;

		double t_l1 = side_length(D,E); 
		double t_l2 = side_length(D,F); 
		double t_l3 = side_length(E,F); 
		double t_area = area(t_l1,t_l2,t_l3);

		get_ang();	
		//printf("CAB = %lf\n", CAB);	
		double ACx = P[C].X - P[A].X;
		double ACy = P[C].Y - P[A].Y;
	
		double ABx = P[B].X - P[A].X;
		double ABy = P[B].Y - P[A].Y;

		double lo = -100000, hi = 100000;

		double p_area;
		for(int i = 0 ; i < 200 ; i++){

			double t = (hi + lo)/ 2;
			
			double hx = (ACx * t) + P[A].X;
			double hy = (ACy * t) + P[A].Y;

			P[H].X = hx;
			P[H].Y = hy;

			P[G].X = P[H].X + ABx;
			P[G].Y = P[H].Y + ABy;
			p_area = para_area();

			//printf("H = %.3lf %.3lf\n", P[H].X , P[H].Y);
			//printf("G = %.3lf %.3lf\n", P[G].X , P[G].Y);
			//printf("area = %.3lf\n", p_area);
			//printf("------------------------------\n");


			if(p_area > t_area){
				hi = t;
			}else{
				lo = t;
			}
		}
//		printf("CAB = %lf\n", CAB);
//		printf("l(A,H) = %lf\n",side_length(A,H) );
//		printf("l(A,B) = %lf\n",side_length(A,B) );
//		printf("%lf %lf\n", lo, hi);
//		printf("%lf %lf\n", t_area, p_area);
//
		cout << fixed << setprecision(3) << P[G].X << " " << P[G].Y << " " << P[H].X << " " << P[H].Y << endl;
	}

	return 0;
}
