#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//
int n;
int idx(int r, int c){
	r--,c--;
	return r*n + c;
}

void debug(ll b){
	ll tmp = b;
	for(int i = 0; i < 32 ; i++){
		printf("%d", (int) b&1);
		b>>=1;
	}
	printf(" %lld\n", tmp);
}

string rot(string B){
	string BR(n * n, '0');
	for(int i = 1 ; i <= n ; i++)
		for(int j = 1 ; j <= n ; j++){
			int k1 = idx(i,j);
			int k2 = idx(j,n-i+1);

			if(B[k1] == '1')
				BR[k2] = '1'; 
		}
	return BR;

}

int main(){
	FASTER;
	while(cin >> n, n){
		int x,y;
		char o;
		map<string,int> S;	
		string B(n*n, '0');
		int found = false;

		for(int i = 0 ; i < 2*n ; i++){
			cin >>x >> y >> o;
			int j = idx(x,y);

			if(o == '+'){
				B[j] = '1';
			}else{
				B[j] = '0';
			}

			string B1 = rot(B);
			string B2 = rot(B1);
			string B3 = rot(B2);
			
			//debug(B);
			//debug(B1);
			//debug(B2);
			//debug(B3);
			//printf("\n");

			//if((S.find(B) != S.end() || S.find(B1) != S.end() ||  S.find(B2) != S.end() || S.find(B3) != S.end() ) && !found){
			if((S.count(B) || S.count(B1)  ||  S.count(B2) || S.count(B3)) && !found){
			//if(S.find(B) != S.end() && !found){
				found = true;
				printf("Player %d wins on move %d\n", (i%2==0 ? 2 : 1),i+1);  
			}

			S[B] = 1;
		}

		if(!found)printf("Draw\n");
	}

	return 0;
}
