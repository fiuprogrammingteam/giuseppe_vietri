#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//
int n;
int ans;
int G[15][15];

void press(int x, int y){
	G[x][y] = 1 - G[x][y];
}

bool check(int r, int c){
	int s = 0;

	if(r)s += G[r-1][c];
	if(r<n-1)s += G[r+1][c];
	if(c)s += G[r][c-1];
	if(c<n-1)s += G[r][c+1];

	return s&1;
}

void bak(int r, int c, int step){
	
	if(step >= ans)return;

	if(c == n){
		c = 0;
		r++;
	}
	
	if(r == 0){
		bak(r,c +1,step);
		if(G[r][c] == 0){
			press(r,c);
			bak(r,c+1,step+1);
			press(r,c);
		}
	}else if(r < n){
		
		if(check(r-1,c)){
			if(G[r][c] == 1)return;

			press(r,c);
			bak(r, c+1,step+1);
			press(r,c);

		}else{
			bak(r,c+1,step);
		}
	}else{

		for(int i = 0 ; i < n ;i++){
			if(check(r-1,i))return;
		}
		ans = min(ans, step);
	}
}

int main(){
	FASTER;
	
	int t;
	cin >> t;
	int Case = 1;
	while(t--){
		cin >> n;

		for(int i = 0 ;i < n ; i++){
			for(int j = 0 ;j < n ; j++){
				cin >> G[i][j];
			}
		}

		ans = 1e9;
		bak(0,0,0);

		cout << "Case " << (Case++) << ": ";
		if(ans == 1e9){
			cout << "-1\n";
		}else
			cout << ans << endl;
	}


	return 0;
}
