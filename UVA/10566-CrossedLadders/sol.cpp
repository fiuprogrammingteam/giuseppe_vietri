#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int main(){
	FASTER;
	
	double x,y,c;
	while(cin >> x >> y >> c){
		double lo = 0, hi = min(x,y);
	
		double mid = 0;
		for(int i = 0 ; i < 500 ; i++){
	
			mid = (lo + hi) * 0.5;
			double b = mid;
			double z = sqrt(x * x - b*b);
			double w = sqrt(y * y - b*b);
			double t = w / (z + w);	
	
			double h = t * z;
		
	
			if(h > c){
				lo = b;
			}else if(h < c){
				hi = b;
			}
		}
		cout << fixed << setprecision(3) << mid << endl;
	}

	return 0;
}
