#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//
int n;

void deb(vi a){

	for(int i = 0 ; i < n ; i++)
		printf("%d ", a[i]);
	printf("\n");
}

vi one(){
	vi a;
	for(int i = 0 ; i < n ; i++)a.push_back(i);
	return a;
}

vi mult(vi a, vi b){
	vi c = a;
	for(int i = 0; i < a.size(); i++){
		c[i] = a[b[i]];
	}
	return c;
}

vi po(vi b, int e){
	if(e == 0)return one();	
	vi res = po(b, e/2);

	res = mult(res,res);

	if(e&1){
		res = mult(res, b);
	}
	return res;
}

int main(){
	FASTER;
	int k;
	while(cin >> n , n){
		string s;	
		vi A(n,0);
		for(int i = 0 ; i < n ; i++){
			cin >> A[i];
			A[i]--;
		}

		while(cin >> k , k){
			getline(cin, s);
			s = s.substr(1);
			while(s.size() < n)s = s + " ";
			vi B = A;
			B = po(A, k);
			string t = s;
			for(int j = 0 ; j < n ; j++){
				t[B[j]] = s[j];
			}
			s = t;
			printf("%s\n", s.c_str());
		}

		getline(cin, s);
		printf("\n");
	}
		
	return 0;
}
