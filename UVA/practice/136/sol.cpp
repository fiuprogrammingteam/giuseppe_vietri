#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<ll> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int main(){
	FASTER;

	priority_queue<ll> q;
	map<ll,int> vis;

	ll n = 1;
	q.push(-1);
	vis[1] = 1;

	vi N;

	int Num = 1500;

	for (int i = 0; i < Num*2; ++i) {

		n = -q.top();
		q.pop();

		N.push_back(n);

//		printf("n = %lld\n",n);

		if(!vis.count(n * 2)){
			vis[n*2]=1;
			q.push(-n*2);
		}
		if(!vis.count(n * 3)){
			vis[n*3]=1;
			q.push(-n*3);
		}
		if(!vis.count(n * 5)){
			vis[n*5]=1;
			q.push(-n*5);
		}
	}

	sort(N.begin(),N.end());

	cout <<"The 1500'th ugly number is "<< N[Num-1] <<"." << endl;


	return 0;
}
