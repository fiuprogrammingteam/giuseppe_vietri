#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int main(){
	FASTER;

	string a,b;

	while(cin >> a >> b){

		if(a == "0" && b == "0")break;

		int n = max(a.size(), b.size());

		while(a.size() < n){
			a = "0"+a;
		}
		while(b.size() < n){
			b = "0"+b;
		}


		reverse(a.begin(), a.end());
		reverse(b.begin(), b.end());

		int ans = 0;
		int c = 0;

		for (int i = 0; i < max(a.size(), b.size()); ++i) {
			int d1 = a[i] -'0';
			int d2 = b[i] -'0';


			if(d1 + d2 + c >= 10){
				ans++;
			}

			c = (d1 + d2 + c) / 10;

		}

		if(ans == 0){
			cout << "No carry operation.\n";
		}else if(ans == 1){
			cout << "1 carry operation.\n";
		}else{
			cout << ans << " carry operations.\n";
		}

	}

	return 0;
}
