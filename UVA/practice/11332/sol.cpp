#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

ll sum(ll n){

	ll s = 0;

	for (int i = 0; i < 10; ++i) {

		s += (n % 10);
		n/=10;

	}
	return s;
}

int main(){
	FASTER;

	ll n;

//	printf("%lf\n", log10(10));
//	return 0;

	while(cin >> n , n){

		while(log10(n)>=1.0-0.000001){
//			printf("n = %d\n",n);
			n = sum(n);

		}


		cout << n << endl;
	}



	return 0;
}
