#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int main(){
	FASTER;

	int n,m,src,tar;
	cin >> n >> m >> tar >> src;

	vector<vi> g(n+1, vi());
	vi deg(n+1,0);
	for (int i = 0; i < m; ++i) {
		int u,v;
		cin >> u >> v;
		g[u].push_back(v);
		g[v].push_back(u);
		deg[u]++;
		deg[v]++;
	}

	vi vis(n+1,0);
	vi cnt(n+1,0);

	queue<int> q;
	q.push(src);
	vis[src]=1;
	bool stay = src != tar;

	while(!q.empty()){

		int u = q.front();
		q.pop();

		for (int i = 0; i < g[u].size(); ++i) {
			int v = g[u][i];
			cnt[v]+=1;
			if(!vis[v] && 2*cnt[v] >= deg[v]){
				q.push(v);
				vis[v]=1;
				if(v == tar)stay = 0;
			}
		}
	}

	if(stay){
		cout << "stay\n";
	}else{
		cout << "leave\n";
	}
	return 0;
}
