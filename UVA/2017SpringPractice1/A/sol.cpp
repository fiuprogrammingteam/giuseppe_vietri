#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int main(){
	FASTER;

	int R, C;
	cin >> R >> C;

	int R2= R - C;
	double a1 = R * R* PI;
	double a2 = R2 * R2 * PI;

//	printf("a1 = %lf\n",a1);
//	printf("a2 = %lf\n",a2);
//
	cout << fixed << setprecision(9) << a2 / a1 * 100 << endl;



	return 0;
}
