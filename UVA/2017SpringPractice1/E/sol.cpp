#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//


string s;

int V[300];

bool is_var(char c){
	return c == 'p' || c == 'q' || c == 'r' || c == 's' || c =='t';
}

bool solve_help(int &i){

	if(is_var(s[i]))return V[s[i++]];


	if(s[i] == 'K'){
		i++;
		int o1 = solve_help(i);
		int o2 = solve_help(i);
		return o1 && o2;
	}

	if(s[i] == 'A'){
		i++;
		int o1 = solve_help(i);
//		i++;
		int o2 = solve_help(i);
		return o1 || o2;
	}

	if(s[i] == 'N'){
		i++;
		int o1 = solve_help(i);
		return !o1;
	}

	if(s[i] == 'C'){
		i++;
		int o1 = solve_help(i);
//		i++;
		int o2 = solve_help(i);
		return !o1 || o2;
	}
	if(s[i] == 'E'){
		i++;
		int o1 = solve_help(i);
//		i++;
		int o2 = solve_help(i);
		return o1 == o2;
	}
	return 0;
}

bool solve(){
	int i = 0;

	return solve_help(i);
}


int main(){
	FASTER;
	while(cin >> s, s !="0"){

		int tau = 1;

		for (int i = 0; i < (1 << 5); ++i) {

			MEM(V,0);

			// p, q, r, s, and t
			if(i&1){
				V['p'] = 1;
			}
			if(i&(1<<1)){
				V['q'] = 1;
			}
			if(i&(1<<2)){
				V['r'] = 1;
			}
			if(i&(1<<3)){
				V['s'] = 1;
			}
			if(i&(1<<4)){
				V['t'] = 1;
			}

			tau &= solve();
		}

		if(tau)
			cout << "tautology\n";
		else
			cout << "not\n";
	}

	return 0;
}
