#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int _r[4] = {1,-1,0,0};
int _c[4] = {0,0,1,-1};

int del_row(char u, int d){

	switch(u){
	case 'u':
		return d * _r[0];
	case 'd':
			return d * _r[1];
	case 'r':
			return d * _r[2];
	case 'l':
			return d * _r[3];
	}
	return 0;
}

int del_col(char u, int d){

	switch(u){
	case 'u':
		return d * _c[0];
	case 'd':
			return d * _c[1];
	case 'r':
			return d * _c[2];
	case 'l':
			return d * _c[3];
	}
	return 0;
}




int main(){
	FASTER;



	int L,W;
	while(cin >> W >> L, L || W){
		int ar,ac;
		int br,bc;


		ar = ac = br = bc = 0;
		int n;
		cin >> n;

		for (int i = 0; i < n; ++i) {

			char c;
			int d;
			cin >> c >> d;


			ar += del_row(c,d);
			ac += del_col(c,d);

			br += del_row(c,d);
			bc += del_col(c,d);


			if(br<0)br=0;
			if(bc<0)bc=0;

			if(br>=L)br=L-1;
			if(bc>=W)bc=W-1;

		}

		printf("Robot thinks %d %d\n", ac,ar);
		printf("Actually at %d %d\n", bc,br);
		printf("\n");
	}


	return 0;
}
