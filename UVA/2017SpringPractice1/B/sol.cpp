#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

char tolo(char a){
	if(a>='A' && a<='Z')return a+32;
	return a;
}

int main(){
	FASTER;
	int n;
	cin >> n;
	string s;
	getline(cin,s);

	for (int i = 0; i < n; ++i) {
		getline(cin,s);

		int A[300];
		MEM(A,0);
		for (int i = 0; i < s.size(); ++i) {
			A[tolo(s[i])]=1;
		}


		int ana = 1;

		for (int i = 'a'; i <= 'z'; ++i) {
			ana &= A[i];
		}

		if(ana){
			cout << "pangram\n";
		}else{
			cout << "missing ";
			for (int i = 'a'; i <= 'z'; ++i) {
				if(!A[i])cout << (char)i ;
			}
			cout << endl;
		}

	}

	return 0;
}
