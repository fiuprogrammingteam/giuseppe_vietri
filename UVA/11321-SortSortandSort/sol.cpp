#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//
int N,M;
int comp(int a, int b){
	int am = a % M;
	int bm = b % M;
	
	if(am == bm){
		int a_odd = abs(a) % 2;
		int b_odd = abs(b) % 2;
		if(a_odd != b_odd){
			return a_odd;
		}
		if(a_odd){
			return a > b;
		}
		return a < b;
	}

	return am < bm;
}

int main(){
	FASTER;
	//printf("%d\n", -100 % 4);
	while(cin >> N >> M, N || M){
		vi A(N,0); 
		for(int i = 0 ; i < N ; i++){
			cin >> A[i];
		}

		sort(A.begin(), A.end(),comp);
		cout << N << " " << M << endl;
		for(int i = 0 ; i < A.size() ; i++){
			cout << A[i] << endl;
		}
	}

	cout << 0 << " " << 0 << endl;

	return 0;
}
