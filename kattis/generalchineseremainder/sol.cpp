#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int main(){
	FASTER;


	int T;
	cin >> T;

	ll a,n,b,m;

	while(T--){

		cin >> a >> n >> b >> m;

		ll p = a;

		int i = 0;

		ll s = a;
		ll l = lcm(n,m);

		bool found = false;

		while(a < l){

			if(a % m == b){
				printf("%lld %lld\n",a,l);
				found=true;
				break;
			}

			a += n;
		}

		if(!found)
			printf("no solution\n");
	}



	return 0;
}
