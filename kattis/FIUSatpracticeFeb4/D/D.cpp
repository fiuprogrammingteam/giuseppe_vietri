#include <bits/stdc++.h>
using namespace std;

const int  INF = std::numeric_limits<int>::max()/3;

#define max(a,b)(a>b?a:b)
#define min(a,b)(a<b?a:b)
#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI (acos(0)*2.0)
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b&(-b)) // Least significant bit

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

// ---------------------------------------------------------------------------------------
ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}


class Edge{
public:
	int u,v,cap,f;

	Edge(int _u, int _v, int _cap) : u(_u),v(_v),cap(_cap){
		f = 0;
	}
	int other(int vertex){
		if(vertex == u)return v;
		else if(vertex == v)return u;
		return -1;
	}

	int residualTo(int vertex){
		if(vertex == u)return f;
		return cap - f;
	}

	void addFlowTo(int vertex, int ff){
		if(vertex == u){
			f -= ff;
		}else if(vertex == v){
			f += ff;
		}
	}
};


typedef vector<Edge*> edgeList;

#define MAX_V 100000
#define MAX_E 100000
const int SRC = 0;
const int SINK = 1;
int Flow;
edgeList parent;
vi visited;
vector<edgeList> adj;

void addEdge(int u, int v, int cap){
	Edge* e = new Edge(u,v,cap);
	adj[u].push_back(e);
	adj[v].push_back(e);
//	printf("Add Edge %d %d %d\n", u, v,cap);
}

bool augmentingPath(){
	queue<int> q;
	parent.assign(MAX_V, NULL);
	visited.assign(MAX_V, 0);

	q.push(SRC);
	visited[SRC] = 1;

	while(!q.empty()){
		int u = q.front();
		q.pop();
		if(u == SINK)
			break;
		for (int i = 0; i < (int)adj[u].size(); ++i) {
			Edge* e = adj[u][i];
			int v = e->other(u);
			if(!visited[v] && e->residualTo(v) > 0){
				visited[v] = 1;
				parent[v] = e;
				q.push(v);
			}
		}
	}
	return visited[SINK];
}

// Ford Fulkerson
int maxFlow(){
	int mf = 0;
	while(augmentingPath()){
		Flow = 1e9;

		for(int v = SINK; v != SRC ; v = parent[v]->other(v)){
			Flow = min(Flow, parent[v]->residualTo(v));
//			printf("%d ",v);

		}
//		printf("\n");
		for(int v = SINK; v != SRC ; v = parent[v]->other(v)){
			parent[v]->addFlowTo(v,Flow);
		}
		mf += Flow;
	}
	return mf;
}


void data1();

int main(){
	 data1();
}


void data1(){

	adj.assign(MAX_V, edgeList());

	int n;
	cin >> n;
	vi A(n,0);
	for (int i = 0; i < n; ++i) {
		cin >> A[i];
	}

	for (int i = 0; i < n; ++i) {
		for (int j = i+1; j < n; ++j) {

			int g =gcd(A[i],A[j]);

			if(g>1){
				addEdge(i+2,j+2,g);
				addEdge(j+2,i+2,g);

			}

		}
	}

	int mini = 0, maxi = 0;

	for (int i = 0; i < n; ++i) {

		if(A[i] < A[mini])mini= i;
		if(A[i] > A[maxi])maxi= i;


	}

	addEdge(SRC,mini+2,1e9);
	addEdge(maxi+2,SINK,1e9);


	cout << maxFlow() << endl;
}
