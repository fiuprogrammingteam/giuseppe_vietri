#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)
#define ALL(v)v.begin(),v.end()
#define PB(v)push_back(v)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//




int win(int d1, int d2 , int d3 , int d4){


	if(d2 > d1){
		int t = d2;
		d2 =d1;
		d1 = t;
	}

	if(d4 > d3){
		int t = d4;
		d4 =d3;
		d3 = t;
	}


	int val1 = d1 * 10 + d2;
	int val2 = d3 * 10 + d4;

//	printf("%d %d %d %d:\n",d1,d2,d3,d4);
//	printf("\tval1 = %d\n",val1);
//	printf("\tval2 = %d\n",val2);


	if(val2 == 21 || val2 == 12)return 0;
	if(val1 == 21 || val1 == 12)return 1;

	if(d1 == d2 && d3 != d4)return 1;
	if(d3 == d4 && d1 != d2)return 0;



	if(d1 == d2 && d3 == d4)return val1 > val2;




	return val1 > val2;
}

int main(){
	FASTER;

	char s0,s1,r0,r1;
	while(cin >> s0 >> s1 >> r0 >> r1){
		if(s0 == '0' && s1 == '0' && r0 == '0' && r1 =='0')break;


		int tot = 0;
		int sum = 0;

		int is0 = (s0 == '*' ? 1 : s0 - '0');
		int is1 = (s1 == '*' ? 1 : s1 - '0');
		int ir0 = (r0 == '*' ? 1 : r0 - '0');
		int ir1 = (r1 == '*' ? 1 : r1 - '0');

		for (int d1 = is0; d1 <= (s0 == '*' ? 6 : is0); ++d1)
			for (int d2 = is1 ; d2 <= (s1 == '*' ? 6 : is1); ++d2)
				for (int d3 = ir0 ; d3 <= (r0 == '*' ? 6 : ir0); ++d3)
					for (int d4 = ir1 ; d4 <= (r1 == '*' ? 6 : ir1); ++d4) {
						tot++;

//						printf("%d %d %d %d => ",d1,d2,d3,d4);
						int w =  win(d1,d2,d3,d4);
//						printf("\tw = %d\n",w);
						sum += w;
					}

		int d = gcd(sum,tot);
		tot/=d;
		sum /=d;

		if(sum == 0)cout << 0 << endl;
		else if(sum == 1 && tot == 1)
			cout << 1 << endl;
		else
		cout << sum << "/" << tot << endl;
//		break;
	}
	return 0;
}
