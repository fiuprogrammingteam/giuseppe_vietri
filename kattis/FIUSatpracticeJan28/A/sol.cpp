#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//


#define MAX_PRIMES 1000000
vi primes;
bitset<MAX_PRIMES> is_prime;

void sieves(){
	is_prime.set();
	for (int i = 4; i < MAX_PRIMES; i+=2)
		is_prime[i] = false;

	for (int i = 3; i*i < MAX_PRIMES; i += 2)
		if(is_prime[i])
			for (int j = i*i; j < MAX_PRIMES; j += i)
				is_prime[j] = false;

	primes.push_back(2);
	for (int i = 3; i < MAX_PRIMES; i += 2) {
		if(is_prime[i])
			primes.push_back(i);
	}
}

ll eulerPhi(ll N){
	ll PF_idx = 0, PF = primes[PF_idx], ans = N;

	while(PF*PF <= N){
		if(N % PF == 0)ans -= ans / PF;
		while(N % PF == 0){N /= PF;}

		PF = primes[++PF_idx];
	}
	if(N != 1 )ans -= ans / N;
	return ans;
}


int main(){
	FASTER;
	ll n;
	 sieves();
	while(cin >> n , n){

		cout << eulerPhi(n) << endl;

	}

	return 0;
}
