#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//
#define MAXN 20

int n;
int x[MAXN], y[MAXN], r[MAXN];

ll dist2(int i, int j){
	ll dx = x[i] - x[j];
	ll dy = y[i] - y[j];
	return dx * dx + dy * dy;
}

bool ok(int i, int j){

	ll r1 = r[i];
	ll r2 = r[j];

	return dist2(i,j) > (r1+r2) * (r1+r2);

}

bool isSubOk(vi s){

	for (int i = 0; i < s.size(); ++i) {
		for (int j = i+1; j < s.size(); ++j) {

			int id1 = s[i];
			int id2 = s[j];

			if(!ok(id1,id2)){

				return false;
			}

		}
	}
	return true;
}

void debug(vi s){
	for (int i = 0; i < s.size(); ++i) {
		printf("%d, ", s[i]);

	}
	printf("\n");
}

ll addR(vi s){
	ll R = 0;
	for (int i = 0; i < s.size(); ++i) {
		R += r[s[i]] * r[s[i]];
	}
	return R;
}


int main(){
	FASTER;

	int t;
	cin >> t;

	while(t--){
		cin >> n;

		for (int i = 0; i < n; ++i) {
			cin >> x[i]  >> y[i] >> r[i];
 		}

//		printf("ok = %d\n", ok(0,1));
		ll ans = 0;

		for (int i = 1; i < 1<<n; ++i) {

			vi sub;

			for (int j = 0; j < n; ++j) {
				if(i & (1<<j)){
					sub.push_back(j);
				}
			}

//			debug(sub);
			if(isSubOk(sub)){
//				printf("debug\n");

				ll tmp = addR(sub);
				if(tmp > ans )ans= tmp;

			}

		}

		cout << ans << endl;

	}

	return 0;
}
