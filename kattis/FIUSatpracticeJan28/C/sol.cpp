#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

int dist[1000][1000];
int main(){
	FASTER;

	vector<string> G;

	int R,C;

	cin >> R >> C;
	G.assign(R,"");
	for (int i = 0; i < R; ++i) {
		cin >> G[i];
	}


	MEM(dist,-1);
	queue<int> rq;
	queue<int> cq;

	dist[0][0] = 0;
	rq.push(0);
	cq.push(0);


	int ans = -1;
	while(!rq.empty()){

		int r = rq.front();
		int c = cq.front();
		int nr,nc;

		rq.pop();
		cq.pop();


		char ch = G[r][c];


//		printf("(%d %d) = %c\n",r,c,ch);

		if(ch == 'N'){
			nr = r -1;
			nc = c;
		}
		if(ch == 'S'){
			nr = r + 1;
			nc = c;
		}
		if(ch == 'W'){
			nr = r ;
			nc = c-1;
		}
		if(ch == 'E'){
			nr = r;
			nc = c+1;
		}
		if(ch == 'T'){
			ans = dist[r][c];
			break;
		}

		if(nr < 0 || nr >= R || nc < 0 || nc>=C){
			cout << "Out\n";
			return 0;
		}


		if(dist[nr][nc] == -1){
//			printf("\t(%d %d) \n",nr,nc);

			rq.push(nr);
			cq.push(nc);
			dist[nr][nc] = dist[r][c] + 1;
		}
	}


	if(ans == -1){
		cout << "Lost\n";
	}else{
		cout << ans << endl;
	}


	return 0;
}
