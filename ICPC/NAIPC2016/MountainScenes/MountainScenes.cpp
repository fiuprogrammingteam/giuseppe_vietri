#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

#define MOD 1000000007
#define MAXN 10001

ll dp[MAXN];

int main(){
	FASTER;


	int n,w, h;

	cin >> n >> w >> h;

	dp[0] = 1;

	for (int j = 0; j < w; ++j) {
		for (int i = n; i >= 0; --i) {
			for (int k = 1; k <= min(h,i); ++k) {
				dp[i] += dp[i-k];
				dp[i] %= MOD;
			}
		}
	}
	ll res = MOD - min(h+1, n/w+1);

	for(auto p : dp){
		res += p;
		res %= MOD;
	}


	cout << res << endl;

	return 0;
}
