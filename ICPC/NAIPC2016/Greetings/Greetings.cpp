#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

/*
 * dp[mask] = min cost to cover letters in mask
 *
 *
 */
#define MAXN 16
#define oo ((ll)(1e17))
int n,K;

int W[MAXN],H[MAXN],Q[MAXN];

ll costEnv[1 << MAXN]; // cost of enclosing letters in one evelop
ll dp[1 << MAXN];



int main(){
	FASTER;

	cin >> n >> K;



	for (int i = 0; i < n; ++i) {
		cin >> W[i] >> H[i] >> Q[i];
	}



	/*
	 * dp[mask] = Cost of using enclosing set 'mask' into one envelop
	 *
	 */
	for (int mask = 1; mask < (1<<n); ++mask) {

		int w,h;
		w = h = 0;

		int cost = 0;
		for (int j = 0; j < n; ++j) {
			if(mask & (1 << j)){
				w = max(w, W[j]);
				h = max(h, H[j]);
			}
		}

		costEnv[mask] = 0;
		for (int j = 0; j < n; ++j) {
			if(mask & (1 << j)){
				costEnv[mask] += 1ll * (w * h - W[j] * H[j]) * Q[j];
			}
		}
	}

	for (int mask = 0; mask < 1<<n; ++mask)dp[mask] = oo;


	/*
	 *
	 * dp[mask] = cost of enclosing set 'mask' into k envelops
	 *
	 */
	dp[0] = 0;
	for (int k = 0; k < K; ++k) {

		for (int mask = (1<<n)-1; mask>=0; --mask) {

			if(dp[mask] == oo)continue;

			int cmask = ((1<<n)-1) ^ mask;

			for (int s = cmask; s > 0; s = (s-1) & cmask) {

				dp[mask | s ] = min(dp[mask | s] , dp[mask] + costEnv[s]);

			}
		}
	}


	cout << dp[(1<<n)-1] << endl;

	return 0;
}
