#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)
#define ALL(v)v.begin(),v.end()
#define PB(a)push_back(a)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//
#define MAXN 2000;
int n,m,k;

vector<vii> G;
ll rem_cost[500];
ll H[500];

ll dp[30][1<< 20];
int conn[50][50];

int main(){
	FASTER;

	cin >> n >> m >> k;

	ll basecost = 0;
	G.assign(m+1,vii());
	MEM(conn,0);
	MEM(rem_cost ,0);
	MEM(H,0);

	for (int i = 0; i < n; ++i) {

		int a,pa,b,pb;
		cin >> a >> pa >> b >> pb;
		a--,b--;
		if(pa < pb){
			G[a].PB(ii(b, pb - pa));
			basecost += pa;
		}else {
			G[b].PB(ii(a, pa - pb));
			basecost += pb;
		}
		conn[b][a] = conn[a][b] = 1;
	}

	for (int u = 0; u < m; ++u) {
		for(auto v: G[u]){
			rem_cost[u] += v.second;
		}
	}


	int h1 = m / 2 , h2 = m - h1;

	for (int u = 0; u < m; ++u) {
		for (int v = 0; v < h1; ++v) {
			if(conn[u][v])
				H[u] |= (1 << v);
		}
	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	for (int mask = 0; mask < (1<<h1); ++mask) {

		// Is this bitmask configuration valid
		bool ok = true;

		for (int j = 0; ok && j < h1; ++j) if(mask&(1<<j)){
			for (int k = 0;ok &&  k < h1; ++k) if(mask&(1<<k)){
				// This means that two connected stores were empty.
				// This case is not possible
				if(conn[j][k])ok = false;
			}
		}

		// Get the cost of this configuration
		ll cost = 0;
		for (int i = 0; i < h1; ++i)if (mask & (1<<i)) cost += rem_cost[i];
		for (int i = 0; i <= h1; ++i) dp[i][mask] = 1ll<<60;

		if(ok)dp[0][mask] = cost;

		for (int rem = 1; rem <= h1; ++rem) {
			dp[rem][mask] = dp[rem-1][mask];
			for (int i = 0; i < h1; ++i) {
				if(mask & (1<<i))
					dp[rem][mask] = min(dp[rem][mask], dp[rem-1][mask ^ (1<<i)]);
			}
		}
	}

	ll ans = 1ll<<60;

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	for (int mask = 0; mask < (1<<h2); ++mask) {

		// Is this bitmask configuration valid
		bool ok = true;

		for (int j = 0; ok && j < h2; ++j) if(mask&(1<<j)){
			for (int k = 0; ok && k < h2; ++k) if(mask&(1<<k)){
				// This means that two connected stores were empty.
				// This case is not possible
				if(conn[h1+j][h1+k]) ok = false;
			}
		}

		if(!ok)continue;

		int exc = 0;
		for (int i = 0; i < h2; ++i) if(mask&(1<<i)) exc |= H[h1+i];
		int used = h2 - __builtin_popcount(mask) + __builtin_popcount(exc);
		if(used > k)continue;

		exc ^= ((1<<h1)-1);

		ll cost = 0;
		for (int i = 0; i < h2; ++i) if (mask & (1<<i)) cost += rem_cost[h1+i];

		for (int i = 0; i <= h1 && i + used <= k ; ++i) {
			ans = min(ans, cost + dp[i][exc]);
		}
	}

	if(ans == 1ll<<60)
		cout << -1 << endl;
	else
		cout << basecost + ans<< endl;
	return 0;
}
