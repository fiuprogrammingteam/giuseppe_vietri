#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)
#define ALL(v)v.begin(),v.end()
#define RALL(v)v.rbegin(),v.rend()
typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//
#define INSZ 3000

int n,k;

vector<vi> G;
int Sala[INSZ];
int Prod[INSZ];


/**
 *
 * p1 + p2 + p3 . .  .
 * __   __ + __          = +
 * s1 + s2 + s3 . . .
 *
 * Suppose that the answer is G
 *
 *
 * then
 *
 * p1 + p2 + p3 ... = G ( s1 + s2 + s3 . . .)
 * (p1 - G*s1) + (p2 - G*s2) + (p3 - G*s3) . . . >= 0
 *
 */

double C[INSZ][INSZ]; // (u,k) maximum number for employee of k terms in subtree u
int TreeSz[INSZ];

void comp(int u, double g){

	for (int i = 0; i < min(k,TreeSz[u])+1; ++i) C[u][i] = -1e100;

	C[u][0] = 0;
	C[u][1] = u ? (Prod[u] - g* Sala[u]) : 0;
	int last = 1;

	for(auto v : G[u]){

		comp(v,g);

		for (int i = min(last,min(k, TreeSz[u])) ; i >= 1; --i) {
			for (int j = 0; j <= min(k, TreeSz[v]) && i+j <= min(k, TreeSz[u]); ++j) {

//				if(u==0){
////					printf("%d\n",min(k, TreeSz[v]));
//					printf("\tC[%d][%d] = %lf\n",u,i+j,C[u][i+j]);
//				}

				C[u][i+j] = max(C[u][i+j], C[u][i] + C[v][j]);
				last = max(last, i+j);
			}
		}
	}
//	for (int i = 0; i <= k; ++i) {
//		printf("\tC[%d][%d] = %lf\n",u,k,C[0][k]);
//	}
}

bool ok(double g){
	comp(0,g);

//	for (int i = 0; i <= k; ++i) {
//		printf("\tC[%d][%d] = %lf\n",0,i,C[0][i]);
//	}
//	printf("\n");
	return C[0][k]>=0.0;
}


void dfs(int u){
	TreeSz[u] = 1;
	for(int  v : G[u]){
		dfs(v);
		TreeSz[u] += TreeSz[v];
	}
//	printf("sz[%d] = %d\n", u , TreeSz[u]);
}

int main(){
	FASTER;

	cin >> k >> n;
	k++;
	G.assign(n+1,vi());


	for (int i = 1; i <= n; ++i) {
		int s,p,r;

		cin >> Sala[i] >> Prod[i] >> r;
		G[r].push_back(i);
	}

	dfs(0);

	double lo =0, hi=1;

	while(ok(hi)){
		lo=hi;
		hi*=2;
	}

	double g=0;
	while (hi-lo > 1e-5) {

		g = (lo+hi)/2.0;
		if(ok(g)){
//			printf("ok!\n");
			lo=g;
		}else{
//			printf("not ok!\n");
			hi=g;
		}
	}

	cout << fixed << setprecision(3) << lo << endl;


	return 0;
}
