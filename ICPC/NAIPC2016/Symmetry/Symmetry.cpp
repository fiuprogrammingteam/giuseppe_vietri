#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)
#define ALL(v)v.begin(),v.end()

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

struct point{
	int x,y;
	bool operator < (const struct point &p1) const{
		if(this->x != p1.x)return this->x < p1.x;
		return this->y < p1.y;
	}
	void debug(){
		printf("[%d %d]\n", this->x, this->y);
	}
};

/**
 * x*a + b*y = c
 */
struct line{
	int a,b,c;

	bool operator < (const struct line &l1) const{
		if(this->a != l1.a)return this->a < l1.a;
		if(this->b != l1.b)return this->b < l1.b;
		return this->c < l1.c;
	}

	bool isinline(const struct point p){
		return p.x * this->a + p.y * this->b  == this->c;
	}
	void debug(){
		printf("[%d %d %d]\n", this->a, this->b,this->c);
	}
};

int n;

void norm(line &l){

	int g = gcd(abs(l.a),abs(l.b));
	g = gcd(g, abs(l.c));

	l.a /= g;
	l.b /= g;
	l.c /= g;


	if(l.a < 0 || (l.a == 0 && l.b < 0) || (l.a == 0 && l.b == 0 && l.c < 0)){
		l.a = -l.a;
		l.b = -l.b;
		l.c = -l.c;
	}
}

line toline(point p1, point p2){
	line l;
	l.a = (p2.y - p1.y);
	l.b = -(p2.x - p1.x);
	l.c = (p1.x*l.a + p1.y*l.b);
	norm(l);
	return l;
}

line perpendicular(point p1, point p2){
	int midx = (p1.x + p2.x) / 2;
	int midy = (p1.y + p2.y) / 2;
	line l;
	l.a = (p2.x - p1.x);
	l.b = (p2.y - p1.y);
	l.c = (midx*l.a + midy*l.b);
	norm(l);
	return l;
}

point getmid(const point &a, const point &b){
	return point{(a.x + b.x)/2, (a.y + b.y)/2};
}

bool point_equal(const struct point &p1,const struct point &p2){
	return p1.x == p2.x && p1.y == p2.y;
}

bool line_equal(const struct line &l1,const struct line &l2){
	return l1.a == l2.a && l1.b == l2.b && l1.c == l2.c;
}

int main(){
	FASTER;

	cin >> n;
	vector<point> P;
	vector<point> mid;
	vector<line> p_lines;
	vector<line> lines;
	int trig[2000000];
	MEM(trig,0);

	for (int i = 1,tr = 0; i <= n; ++i) {
		tr+=i;
		trig[tr] = i;
	}


	for (int i = 0; i < n; ++i) {
		int x, y;
		cin >> x >> y;
		x *= 2;
		y *= 2;
		P.push_back(point{x,y});
	}

	for (int i = 0; i < n; ++i) {
		for (int j = i+1; j < n; ++j) {
			mid.push_back(getmid(P[i], P[j]));
			p_lines.push_back(perpendicular(P[i], P[j]));
			lines.push_back(toline(P[i], P[j]));
		}
	}

	sort(ALL(P));
	sort(ALL(mid));
	sort(ALL(p_lines));
	sort(ALL(lines));


	if(P.size() <=2 ){
		cout <<  0 << endl;
		return 0;
	}

	int ans = 2;

	int count = 2;
	// Symmetric around point
	point last = mid[0];
//	last.debug();
	for (int i = 1; i < mid.size(); ++i) {
		point cur = mid[i];
//		printf("count = %d\n", count);
//		last.debug();
//		cur.debug();
		if(point_equal(last, cur)){
			count+=2;
		}else{
			auto it = lower_bound(ALL(P), last);
			if(it!=P.end() && point_equal(*it, last))count++;
			if(count > ans) ans = count;

			count = 2;
		}
		last = cur;
	}

//	printf("ans = %d\n", ans);
	// perpendicular
	line lastline = p_lines[0];
	count = 2;
	for (int i = 1; i < p_lines.size(); ++i) {

		line l = p_lines[i];

		if(line_equal(lastline, l)){
			count+=2;
		}else{
			for(auto p : P)count += lastline.isinline(p);
			if(count > ans) ans = count;
			count = 2;
		}
		lastline = l;
	}


//	printf("ans = %d\n", ans);

	lastline = lines[0];
	count = 1;
//	lastline.debug();
	for (int i = 1; i < lines.size(); ++i) {

		line l = lines[i];
//		printf("count = %d -------\n",count);
//		l.debug();
		if(line_equal(lastline, l)){
			count+=1;
		}else{
			count = trig[count] + 1;
			if(count > ans) ans = count;
			count =1;
		}


		lastline = l;
	}

	if(P.size() <= 2)ans = P.size();

	cout << ((int)P.size()) - ans << endl;
	return 0;
}
