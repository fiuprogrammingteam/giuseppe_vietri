#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

#define MAXSZ 200100
#define LOGT 20


vector<vi> T;
vi inTree;

int P[MAXSZ][LOGT];
int H[MAXSZ];
int root;
void create(int u, int p = 0){

	H[u] = H[p] + 1;

	P[u][0] = p;

	for (int i = 1; i < LOGT; ++i) {
		P[u][i] = P[P[u][i-1]][i-1];
	}

	for (int i = 0; i < T[u].size(); ++i) {
		int v = T[u][i];
		if(v!=p){
			create(v,u);
		}
	}
}

int ca(int u, int v){

	if(H[u] > H[v]){
		int t = u;
		u = v;
		v = t;
	}

	// Make them have the same height
	while(H[v] > H[u]){
		for (int i = 0; i < LOGT - 1; ++i) {
			if(H[P[v][i+1]] < H[u]){
				v = P[v][i];
				break;
			}
		}
	}

	// Find common ancestor
	while(u != v){
		for (int i = 0; i < LOGT - 1; ++i) {
			if(P[v][i+1] == P[u][i+1]){
				v = P[v][i];
				u = P[u][i];
				break;
			}
		}
	}

	return u;
}

int len(int u, int v){
	return H[u] + H[v] - 2 * H[ca(u,v)] + 1;
}


int main(){
	FASTER;

	int n;
	cin >> n;

	T.assign(n+1, vi());
	inTree.assign(n+1,0);
	MEM(P,0);
	MEM(H,0);


	root = n + 1;
	for (int i = 0; i < n-1; ++i) {
		int u,v;
		cin >> u >> v;

		T[u].push_back(v);
		T[v].push_back(u);

		inTree[u] = inTree[v] = 1;

		root = min(root, u);
		root = min(root, v);
	}

	create(root);

	ll ans = 0;

	for (int u = 1; u <= n/2+1; ++u) {

		if(!inTree[u])continue;

		for (int v = 2 * u; v <= n; v += u) {
			if(inTree[v]){
				int l = len(u,v);
				ans += l;
			}
		}
	}

	cout << ans << endl;

	return 0;
}
