#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)
#define ALL(v)v.begin(),v.end()
#define PB(v)push_back(v)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//
#define MAXN 1000010
#define MAXK 300001

int n,k;

ll dp[350][MAXK];
vi B[MAXN];
vi S;

bool rev(int a, int b){
	return a > b;
}

int main(){
	FASTER;

	cin >> n >> k;

	for (int i = 0; i < n; ++i) {
		int s,v;
		cin >> s >> v;
		if(B[s].size() == 0)
			S.PB(s);
		B[s].PB(v);
	}


	int buckets = S.size();

	sort(ALL(S),rev);
	S.PB(0);
	reverse(ALL(S));

	for (int i = 1; i <= buckets; ++i) {
		sort(ALL(B[S[i]]), rev);
	}



	for (int j = 0; j <= k; ++j)dp[0][j] = -1e9;
	dp[0][0] = 0;

	for (int i = 1; i <= buckets; ++i) { // 300

		// set to oo
		for (int j = 0; j <= k; ++j) dp[i][j] = -1e9;

		int sz = S[i];
		vi values = B[sz];

		for (int j = 0; j <= k ; ++j) { // 100,000

			dp[i][j] = dp[i-1][j];
			if(j && dp[i][j-1] > dp[i][j])
				dp[i][j] = dp[i][j-1];

			int sum = 0;
			int val = 0;

			for (int x = 0; x < values.size(); ++x) { // 100,000

				sum += S[i];
				val += values[x];


				if(j-sum < 0)break;

				ll tmp = dp[i-1][j-sum] + val;

//				if(tmp < dp[i][j])break;

				dp[i][j] = max(dp[i][j], tmp);
			}
		}
	}

	for (int i = 1; i <= k; ++i) {
		cout << dp[buckets][i] << " ";
	}
	cout << endl;
	return 0;
}
