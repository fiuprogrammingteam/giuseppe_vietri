#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//

#define MAXN 1000100

int A[MAXN];

bitset<MAXN> st;
int lazy[MAXN];


void upt(int p, int l, int r, bitset<MAXN> b){

	if(l == r){
		A[l] += b[0];

	}

	if(lazy[p]){

		if(l != r){

			lazy[p*2] = 1;
			lazy[p*2+1] = 1;

			st[p * 2 + 1];
			st[p * 2 + 1];

		}

	}

}



int main(){
	FASTER;


	return 0;
}
