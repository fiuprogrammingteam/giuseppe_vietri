#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * __builtin_popcountll(long long d)
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//
#define MAXN 2200

class cir{
public:

	int x,y,r,a,b;
	ll r2;
	int id;

	int score[MAXN];

	ll bestscore = 0;

	int minanc = 0;

	int candidate;


	int count_anc;

	int chosen;

	cir(int _x,int _y, int _r , int _a , int _b, int i){
		x = _x;
		y = _y;
		r = _r;
		a = _a;
		b = _b;
		id = i;

		r2 = 1ll * r * r;

		chosen = 0;
	}

	bool operator < (const cir c){
		return r < c.r;
	}

	void debug(){
		printf("%d %d %d %d %d\n",x,y,r,a,b);
	}
};

int n;

vector<vi> child;
vector<cir> circles;
vi P;
vi chosen;

ll dist2(int i, int j){
	ll dx = circles[i].x - circles[j].x;
	ll dy = circles[i].y - circles[j].y;
	return dx * dx + dy * dy;
}

ll getscore(int u, int no_anc){


	cir &c = circles[u];

	c.bestscore = 0;

	c.score[0] = 0;

//	printf("======================================================\n");

	for(int i = 1 ; i <= no_anc ; i++){

		c.score[i] = c.score[i-1] + ((i&1) ? c.a : c.b);
		if(c.score[i] > c.bestscore){
			c.bestscore = c.score[i];
			c.minanc = i;
		}
	}

	c.count_anc = no_anc;


	ll tot = c.bestscore;

	for(auto v : child[u]){

		tot += getscore(v, no_anc+1);

	}

	return tot;
}


bool choose(int u, int bef){

	cir &c = circles[u];


	c.candidate = false;


	bool anc_ok = true;

	if(!c.chosen){

		int aft =  c.count_anc - bef;

		c.candidate = c.score[aft] == c.bestscore;

		anc_ok = aft > c.minanc;
	}else{
		bef++;
	}


	bool child_allow = true;

	for(auto v : child[u]){
		child_allow &= choose(v, bef);
	}

	c.candidate &= child_allow;

	return child_allow && anc_ok;
}



int main(){
	FASTER;

	int T;
	cin >> T;


	while(T--){

		cin >> n;
		circles.clear();
		for (int i = 0; i < n; ++i) {
			int x,y,r,a,b;
			cin >> x >> y >> r >> a >> b;
			cir ci(x,y,r,a,b, i+1);
			circles.push_back(ci);
		}

		sort(circles.begin(),circles.end());

		child.assign(n,vi());
		P.assign(n, -1);
		chosen.assign(n, 0);


		for (int i = 0; i < n; ++i) {
			for (int j = i+1; j < n; ++j) {


				if(dist2(i,j) < circles[j].r2){

					child[j].push_back(i);
					P[i] = j;

					break;
				}

			}
		}

		ll tot = 0;;


		for(auto i = 0 ; i < n ; i++){
			if(P[i] == -1){
				tot += getscore(i, 0);
			}
		}

		cout << tot << endl;


		for(int it = 0 ; it < n ; it++){

			for(auto i = 0 ; i < n ; i++){
				if(P[i] == -1){

					choose(i,0);
				}
			}

			int ind = n+1;
			int pos = 0;
			for(int i = 0 ; i < n ; i++){

				auto c = circles[i];
				if(c.candidate && c.id< ind){
					ind = c.id;
					pos = i;
				}
			}

			circles[pos].chosen = 1;

			cout << (it ? " "  : "");
			cout << ind;
		}
		cout << endl;
	}
	return 0;
}
